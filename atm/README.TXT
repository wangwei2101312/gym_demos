=============================================================================================================================

 ATM Demo
    Copyright: Zoltan Richter
    version: 1.0.1
    last modified: 2017.03.16.
 
-----------------------------------------------------------------------------------------------------------------------------

 1. Provides an ATM library, able to call a service provider, which provides single responsibility services, based on a configurable state machine 
 
=============================================================================================================================
  Implemented idoms:
-----------------------------------------------------------------------------------------------------------------------------

   2.1. Factory = IAtmFactory -> AtmFactory: provides ATM and ATM dumper instances for external use and ATM cookie for internal use.
   2.2. State machine = AtmServiceProvider
   2.3. Dispatcher (visitor) = ATM and bank dispatching methods provided by service provider to ATM services
   2.4. Strategy = IAtmStrategy -> AtmStrategy_Asc, AtmStrategy_Desc, AtmStrategy_Mixed - for handling ATM store manipulation transparently
   2.5. Listener (observer) = IAtmListener, IAtmDumperListener
   2.6. Proxy
   2.7. Builder
   
=============================================================================================================================
 SOLID principles
-----------------------------------------------------------------------------------------------------------------------------

   3.1. SRP -> each ATM task executed by a dedicated service. Each service solves one step only 
   3.2. OCP -> To add a new service, no need to chnage ATM or existent services. Simply create the service, implemnenting the IAtmService interface and resolve AtmServiceProvider to get allocated and included into the state machine handled schedule
   3.3. LSP -> No dummy methods after inheritance
   3.4. ISP -> see IAtm, IAtmDumper or IAtmService, IAtmCookie
   3.5. DIP -> overall

   Others:
   - no "God" classes/methods
   - consistent and clear coding guideline, with doxygen comments
    
=============================================================================================================================
 Supported platforms
-----------------------------------------------------------------------------------------------------------------------------

  4.1 Supported OS/Architecture
  
     - Windows 32|64 - tested on Windows 7 64 bit (tested with MSVC 2010)
     - Linux 32|64   - tested on Gentoo Linux 32|64 bit, Debian Linux 32|64 bit

  4.2 Supported C++ standards:

     - C++98, or later - tested for  C++98, C++03, C++11

     
=============================================================================================================================
 Prerequisities
-----------------------------------------------------------------------------------------------------------------------------

 *5.1. CMake 2.7 or later 

  5.2. GTest - Google test framework (for unit test) - already included in the "externals" subfolder. To build GTest from the externals folder, please use the CMake option ATM_GTEST_MODE="internal"

 *5.3. A C++ compiler, supporting at least the 98 standard:
       - gcc 4.9 or later (Linux)
       - msvc 2008, or later (Windows)

 *5.4. STL library, std=C++98 or later

  5.5. (Optional) valgrind for memory tests
 

*)= mandatory, to can build the application. 
   
   When non mandatory elements are missing, the build system shall skip the affected sub-modules 
   Ex: no unit test will be built, when GTest is missing
    
   
=============================================================================================================================
6. Installing the sources
-----------------------------------------------------------------------------------------------------------------------------

6.1. Download, or checkout the archive from a public GitLab 
     repository also:
       https://gitlab.com/wangwei2101312/gym_demos/  [atm]
     
6.2. Extract the archive - when everything went well, there should appear an "atm" sub-folder with the content:
     - "source" sub-folder - contains the C++ source code for:
         i)  atm library, in sub-folder "atmlib" 
         ii) atm command line appliucation, in sub-folder "atmapp"
     - "tests" sub-folder - contains: test
         i)  unit test, in sub-folder "unit" (GTest)
     - "CMakeLists.txt" the root cmake file of the solution
     - "README.TXT" the current Readme

         
=============================================================================================================================
7. Building the sources
-----------------------------------------------------------------------------------------------------------------------------
         
7.1. Windows
  
  7.1.1. Use the CMake GUI which generates a CMake based solution
           i)   set as "Source" the "atm" folder
           ii)  set as "Build" a new folder - ex "build" inside "atm"
           iii) run "Configure", than "Generate" (select a Visual Studio Generator) - the generated solution files will be placed in the "Build" directory (ex "atm.sln" for MSVC)
           iv)  when MSVC generatori is used and GTest is built from the "externals" folder (cmake option ATM_GTEST_MODE="internal") - please run 2 times the "Generate" option, otherwise MSVC amy encounter linkage errors with the MSVC Runtime)

  7.1.2. Build the solution
          - Open the generated build files with Visual Studio (or the selected generator's IDE) and compile/build 

          - The atmlib static library is configured to be built into the "lib" sub-folder 
            (by MSVC under Debug/Release/x64/.. subfolders - according to build type)

          - The atm-terminal command line application is configured to be built into the "bin" sub-folder
            (by MSVC under Debug/Release/x64/.. subfolders - according to build type)
            
          - When dynamic library build is selected, the atmlib.dll is copied into the "bin" subfolder also
          
7.2. Linux

  7.2.1. Use the CMake GUI/command line, or use KDeveloper/QT Creator/... IDE-s, which can generate CMake based make files
           i)   set as "Source" the "atm" folder
           ii)  set as "Build" a new folder - ex "build" inside "atm"
           iii) run "Configure", than "Generate" (or goto atm folder, run " mkdir build & cd ./build && cmake ../ from bash)

  7.2.2. Build the solution
          - Open the generated build files with KDevelop/QT Creator/... (or run simply "make" in bash, inside the "build" folder)

          - The atmlib static library is configured to be built into the "lib" sub-folder 

          - The atm-terminal command line application is configured to be built into the "bin" sub-folder
            
          - When dynamic library build is selected, the atmlib.so is copied into the nBin" subfolder also
           - The atmlib static library is configured to be built into the "lib" sub-folder 
          

=============================================================================================================================
8. Usage of the ATM command line application
-----------------------------------------------------------------------------------------------------------------------------

 8.1. Run from command line with the -h option, it displays a help for how to use it         

        atm-terminal -h

 
=============================================================================================================================
9. Running the unit tests
-----------------------------------------------------------------------------------------------------------------------------

 9.1. In the "tests/unit" folder we have a draft GTest implementation for the stmlib library
       
 9.2. We can run in command line the "atmlib-utest" application and check the results, or to use "make test" under Linux
 
 Known issues Windows:
  - On Windows the GTest localization by CMake depends from the envrionment variables and available GTest installations. 
    Even when GTest is localized, it might happen we need to change some compiler/build options of the unit test 
    application - ex: "Multi-threaded DLL /MD" -> "Multi-threaded /MT"
  - In order to avoid the issues above, there exists the source of GTest in the "externals" folder and by default it is included in the build system  
 
=============================================================================================================================
10. Suggested improvements for the future
-----------------------------------------------------------------------------------------------------------------------------

10.1. Add support to more languages (see AtmStrings.h/cpp and AtmServiceLng.cpp )

10.2. Finalize printer service

10.3. Add a graphical variant for client application - for QT and Android 

10.4. Finalize Unit tests, to get 100% coverage

10.5 Create integration tests with scripting - ex Python 2.7

