////////////////////////////////////////////////////////////////////////////////////////////////////
// MZ/X license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For MZ/X - smart traveler assistant software package
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  RoTS-Bit
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include <map>
#include "atmstrategy_asc.h"
#include "atmstrategy_desc.h"
#include "atmstrategy_mix.h"


using namespace atm;


///////////////////////////////////////////////////////////////////////////////////////////////////
/** \brief Ascendant ATM strategy
  */
TEST( atmstrategy, ascendant ) {
    std::map<int, int > store;
    AtmStrategyAsc strategy;
    ASSERT_EQ( 0 , strategy.extract( store, 100 ) );
    ASSERT_EQ( 0 , store.size() );
    store[10] = 0;
    store[20] = 0;
    store[30] = 2;
    store[40] = 0;
    store[50] = 5;
    store[100] = 3;
    store[200] = 5;
    store[300] = 0;
    ASSERT_EQ( 30, strategy.extract( store, 120 ) );
    ASSERT_EQ( 30, strategy.extract( store, 120 ) );

    ASSERT_EQ( 50, strategy.extract( store, 120 ) );
    ASSERT_EQ( 50, strategy.extract( store, 120 ) );
    ASSERT_EQ( 50, strategy.extract( store, 120 ) );
    ASSERT_EQ( 50, strategy.extract( store, 120 ) );
    ASSERT_EQ( 50, strategy.extract( store, 120 ) );

    ASSERT_EQ( 0, strategy.extract( store, 00 ) );

    ASSERT_EQ( 100, strategy.extract( store, 120 ) );
    ASSERT_EQ( 100, strategy.extract( store, 120 ) );
    ASSERT_EQ( 100, strategy.extract( store, 120 ) );

    ASSERT_EQ( 0, strategy.extract( store, 120 ) );
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/** \brief Descendant ATM strategy
  */
TEST( atmstrategy, descendant ) {
    // Test 'sizes
    std::map<int, int > store;
    AtmStrategyDesc strategy;
    ASSERT_EQ( 0 , strategy.extract( store, 100 ) );
    ASSERT_EQ( 0 , store.size() );
    store[10] = 3;
    store[20] = 0;
    store[30] = 2;
    store[40] = 0;
    store[50] = 5;
    store[100] = 3;
    store[200] = 5;
    store[300] = 0;

    ASSERT_EQ( 100, strategy.extract( store, 120 ) );
    ASSERT_EQ( 100, strategy.extract( store, 120 ) );
    ASSERT_EQ( 100, strategy.extract( store, 120 ) );

    ASSERT_EQ( 50, strategy.extract( store, 120 ) );
    ASSERT_EQ( 50, strategy.extract( store, 120 ) );
    ASSERT_EQ( 50, strategy.extract( store, 120 ) );
    ASSERT_EQ( 50, strategy.extract( store, 120 ) );
    ASSERT_EQ( 50, strategy.extract( store, 120 ) );

    ASSERT_EQ( 30, strategy.extract( store, 120 ) );
    ASSERT_EQ( 30, strategy.extract( store, 120 ) );

    ASSERT_EQ( 10, strategy.extract( store, 120 ) );
    ASSERT_EQ( 10, strategy.extract( store, 120 ) );
    ASSERT_EQ( 10, strategy.extract( store, 120 ) );

    ASSERT_EQ( 0, strategy.extract( store, 120 ) );
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/** \brief Mixed ATM strategy
  */
TEST( atmstrategy, balanced ) {
    std::map<int, int > store;
    AtmStrategyMixed strategy;
    ASSERT_EQ( 0 , strategy.extract( store, 100 ) );
    ASSERT_EQ( 0 , store.size() );
    store[10] = 3;
    store[20] = 0;
    store[30] = 20;
    store[40] = 0;
    store[50] = 10;
    store[100] = 3;
    store[200] = 5;
    store[300] = 0;

    ASSERT_EQ( 30, strategy.extract( store, 120 ) );
    ASSERT_EQ( 30, strategy.extract( store, 120 ) );
    ASSERT_EQ( 30, strategy.extract( store, 120 ) );
    ASSERT_EQ( 30, strategy.extract( store, 120 ) );

    ASSERT_EQ( 50, strategy.extract( store, 120 ) );
    ASSERT_EQ( 30, strategy.extract( store, 120 ) );
}
