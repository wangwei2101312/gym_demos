////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __IATM_H__
#define __IATM_H__

namespace atm {
      
/** \brief ATM interface class
  *  Provides access to an ATM instance
  */ 
class IAtm {
public:    
    /** \brief Strategy for operating ATM store
      */ 
    enum EStrategy {
        eS_Descendant = 0, // Tries to extract greater banknotes at first
        eS_Ascendant,      // Tries to extract lower banknotes at first 
        eS_Balanced        // Tries to keep in balance the ATM store
    };

protected:    
    /** \brief Virtual destructor
      */ 
    virtual ~IAtm() {}
    
public:    
    /** \brief Initialize the ATM
      */ 
    virtual void init() = 0;
    
    /** \brief Go back to the previous stage
      */ 
    virtual void back() = 0;
        
    /** \brief Go to the next stage
      * \param[in] input - user input
      * \param[out] result - ATM answer
      */ 
    virtual void next( const char* input,
                       const char*& result ) = 0; 

    /** \brief Exit, with canceling any pending operations
      */ 
    virtual void exit() = 0;
    
public:
    /** \brief Checks if ATM is in active mode
      * \returns true, when ATM session is valid, otherwise false
      */ 
    virtual bool isActive() const = 0;
    
    /** \brief Checks if ATM has a valid previous state
      * \returns true, when ATM previous state is valid, otherwise false
      */ 
    virtual bool hasBack() const = 0;

    /** \brief ATM's current state name
      * \returns the name of current state
      */ 
    virtual const char* getStateName() const = 0;    
    
    /** \brief ATM's current state's prompt
      * \returns the prompt ofr the current state
      */ 
    virtual const char* getStatePrompt() const = 0;    

    /** \brief ATM's current's state options count
      * \returns the prompt ofr the current state
      */ 
    virtual int getStateOptionsCount() const = 0;    
    
    /** \brief ATM's current's state option by index
      * \param[in] index - option index
      * \returns option text
      */ 
    virtual const char* getStateOption(int index) const = 0;    
};

}// namespace atm

#endif// IATM_H
