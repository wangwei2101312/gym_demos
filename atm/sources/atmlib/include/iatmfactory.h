////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __IATM_FACTORY_H__
#define __IATM_FACTORY_H__

#include <atm_config.h>
#include <iatm.h>

namespace atm {

/// Forward declarations
class IAtmListener;
class IAtmDumper;
  
/** \brief ATM factory interface class
  *  Provides access to an ATM instance
  */ 
class IAtmFactory {
protected:
    /** \brief Virtual destructor
      */     
    virtual ~IAtmFactory() {}
    
public:
    /** \brief Provides access to ATM factory
      * \return pointer to a valid ATM interface when succeed, otherwise NULL
      */     
    ATM_API static IAtmFactory* getInstance();

public:    
    /** \brief Provides access to an ATM instance
      * \param[in] listener - ATM listener object
      * \param[in] index - ATM index
      * \param[in] strategy - strategy to operate ATM store
      * \return pointer to a valid ATM instance, when succeed, otherwise NULL
      */ 
    virtual IAtm* getAtm( IAtmListener& listener,
                          int index,
                          IAtm::EStrategy strategy = IAtm::eS_Balanced ) = 0;
    
    /** \brief Provides access to an ATM dumper instance
      * \return pointer to a valid ATM dumper instance, when succeed, otherwise NULL
      */ 
    virtual IAtmDumper* getAtmDumper() = 0;
};

}// namespace atm

#endif// IATM_FACTORY_H
