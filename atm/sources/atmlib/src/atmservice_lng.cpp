////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atmservice_lng.h"
#include "iatmcookie.h"
#include "atmbank.h"
#include <stdlib.h>
#include <string.h>

namespace atm {

namespace {
    const char* s_name   = "LANGUAGE SELECTION"; 
    const char* s_prompt = "Please select your language"; 
    const char* s_options[] = { "English", "Romana", "Magyar" }; 
    // const int   s_option_count = 3; switch back when Hungarian language strings are avialable
    const int   s_option_count = 2;
    const char* s_invalid_lang = "Invalid language selection. Please try again"; 
    const char* s_valid_lang = "Language selected... "; 
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServiceLng::AtmServiceLng() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServiceLng::~AtmServiceLng() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmServiceLng::resetOptions( IAtmCookie& cookie,
                                  IAtmListener& listener ) {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServiceLng::execute( IAtmCookie& cookie,
                             IAtmListener& listener,   
                             const char* input,
                             const char*& result ) {
    bool success = false;
    if ( ( NULL != input ) && 
         ( atoi( input ) > 0 ) && 
         ( atoi( input ) <= s_option_count ) ) {
        cookie.setProperty( IAtmCookie::eLanguage, input );
        success = true;
        result = s_valid_lang;
    }
    else {
        result = s_invalid_lang;
    }
    return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServiceLng::getName( const IAtmCookie& cookie ) const {
    return s_name;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServiceLng::getPrompt( const IAtmCookie& cookie ) const {
   return s_prompt;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
int AtmServiceLng::getOptionsCount( const IAtmCookie& cookie ) const {
    return s_option_count;
}
 
 
////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServiceLng::getOption( const IAtmCookie& cookie,
                                       int index ) const {
    if ( index < s_option_count ) {
        return s_options[ index ];
    }
    return NULL;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const int AtmServiceLng::getSelectedOption( const IAtmCookie& cookie ) const {
    // ATM service card has no selected options
    return -1;
}

}// namespace atm