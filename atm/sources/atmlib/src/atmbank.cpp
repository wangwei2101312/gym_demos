////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atmbank.h"
#include <iatmdumplistener.h>
#include <string.h>
#include <stdio.h>

namespace atm {


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmBank* AtmBank::getInstance() {
    static AtmBank s_bank;
    return &s_bank;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmBank::AtmBank() {
    // John Doe
    m_accounts.push_back( Account( AtmBankAccount( "John Doe", 2700, 10000, 500 ) ) );
    // Card 1
    m_accounts.rbegin()->cards.push_back( Card( "1234567890", "4321", 3000, 2000 ) );
    // Card 2
    m_accounts.rbegin()->cards.push_back( Card( "0123456789", "5432", 5000, 9000 ) );

    // Mary Smith
    m_accounts.push_back( Account( AtmBankAccount( "Mary Smith", 700, 1000, 0 ) ) );
    // Card 1
    m_accounts.rbegin()->cards.push_back( Card( "3456789012", "6543", 1000, 2000 ) );
    // Card 2
    m_accounts.rbegin()->cards.push_back( Card( "6789012345", "7654", 500, 100 ) );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmBank::~AtmBank() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmBank::checkCardId( const char* cardid ) const {
    const char*         l_pin;
    AtmBankAccount*     l_account;
    const int*          l_maxIn; 
    const int*          l_maxOut; 
    return findAccount( cardid, l_pin, l_account, l_maxIn, l_maxOut );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmBank::checkPin( const char* cardid,
                        const char* pin ) const {
    const char*         l_pin;
    AtmBankAccount*     l_account;
    const int*          l_maxIn; 
    const int*          l_maxOut; 
    if ( findAccount( cardid, l_pin, l_account, l_maxIn, l_maxOut ) ) {
        return ( 0 == strcmp( pin, l_pin ) );
    }
    return false;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmBank::extract( const char* cardid, 
                       const char* pin,
                       int value,
                       bool simulate ) {
    const char*         l_pin;
    AtmBankAccount*     l_account;
    const int*          l_maxIn; 
    const int*          l_maxOut; 
    if ( findAccount( cardid, l_pin, l_account, l_maxIn, l_maxOut ) ) {
        if ( 0 == strcmp( pin, l_pin ) ) {
            return l_account->extract( value, simulate );
        }
    }
    return false;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmBank::showInfo( const char* cardid, 
                        const char* pin,
                        char type,
                        std::vector< int >& info ) {
    const char*         l_pin;
    AtmBankAccount*     l_account;
    const int*          l_maxIn; 
    const int*          l_maxOut; 
    if ( findAccount( cardid, l_pin, l_account, l_maxIn, l_maxOut ) ) {
        if ( 0 == strcmp( pin, l_pin ) ) {
            return l_account->showInfo( info, type );
        }
    }
    return false;

}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmBank::findAccount( const char* cardid,
                           const char*& pin,
                           AtmBankAccount*& account, 
                           const int*& maxIn,
                           const int*& maxOut ) const {
    // Search card id
    for ( std::list<Account>::iterator itAcc = m_accounts.begin(); itAcc != m_accounts.end(); ++itAcc ) {
        for ( std::list<Card>::const_iterator itCard = itAcc->cards.begin(); itCard != itAcc->cards.end(); ++itCard ) {
            if ( 0 == itCard->cardid.compare( cardid ) ) {
                account = &( itAcc->account );
                pin = itCard->pin.c_str();
                maxIn = &( itCard->maxIn );
                maxOut = &( itCard->maxOut );
                return true;
            }
        }
    }
    return false;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmBank::dump( IAtmDumpListener& listener ) const {
    int pos = 0;
    char key[64];
    char val[64];
    listener.dump( "BANK status", NULL, 0 );
    // Search card id
    for ( std::list<Account>::iterator itAcc = m_accounts.begin(); itAcc != m_accounts.end(); ++itAcc ) {
        sprintf( key, "%d. <Account>", ++pos );
        listener.dump( key, NULL, 1 );
        listener.dump( "Cards", NULL, 2 );
        int card = 0;
        for ( std::list<Card>::const_iterator itCard = itAcc->cards.begin(); itCard != itAcc->cards.end(); ++itCard ) {
            listener.dump( "Card Id", itCard->cardid.c_str(), 3 );
            listener.dump( "Pin", itCard->pin.c_str(), 3 );
        }
        itAcc->account.dump( listener, 3 );
    }
}


}// namespace atm
