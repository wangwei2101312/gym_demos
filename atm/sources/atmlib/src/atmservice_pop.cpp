////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atmservice_pop.h"
#include "iatmcookie.h"
#include "atmstrings.h"
#include "iatmlistener.h"
#include <stdlib.h>
#include <string>
#include <string.h>
#include <stdio.h>

namespace atm {

namespace {
    const int s_option_count = 3;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServicePop::AtmServicePop( DispatchBank bank,
                              DispatchAtm  atm )
: m_bank( bank )
, m_atm( atm ) {    
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServicePop::~AtmServicePop() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmServicePop::resetOptions( IAtmCookie& cookie,
                                  IAtmListener& listener) {
    if ( NULL != cookie.getProperty( IAtmCookie::eCashOut ) ) {
        int idx = 0;
        std::string banknote;
        std::string action( atm_string::getString( cookie, atm_string::id_abort_cash ) );
        action += " | ";
        while ( cookie.getProperty( IAtmCookie::eCashOut, idx++, banknote ) ) {
            // Restore to ATM storage
            ( *m_atm )( cookie, -atoi( banknote.c_str() ) );
            action += banknote;
            action += " | ";
        }
        cookie.setProperty( IAtmCookie::eCashOut, NULL );
        listener.prompt( NULL, action.c_str() );
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServicePop::execute_extract( IAtmCookie& cookie,
                                     IAtmListener& listener ) {
    bool success = false;
    int  amount = 0;
    int  extracted = 0;
    char buff[ 10 ];
    if ( NULL != cookie.getProperty( IAtmCookie::eAmountVal ) ) {
        // Use amount as input
        amount = atoi( cookie.getProperty( IAtmCookie::eAmountVal ) );
        // Extract first banknote
        int banknote = ( *m_atm )( cookie, ( amount - extracted ) ); 
        while ( ( amount > extracted ) && ( banknote > 0 ) ) {
            extracted += banknote;
            // Store banknote
            sprintf( buff, "%u", banknote );
            cookie.addProperty( IAtmCookie::eCashOut, buff );
            // Extract next banknote
            if ( ( amount - extracted ) ) {
                banknote = ( *m_atm )( cookie, ( amount - extracted ) );
            }
        }
        if ( extracted == amount ) {
            // We are ready - notify bank for transaction
            if ( ( *m_bank )( cookie.getProperty( IAtmCookie::eCardId ), 
                              cookie.getProperty( IAtmCookie::ePin ), 
                              extracted ) ) {
                success = true;
            }
        }
    }
    return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServicePop::execute_eject( IAtmCookie& cookie,
                                   IAtmListener& listener ) {
    bool success = false;
    int idx = 0;
    std::string banknote;
    std::string action( atm_string::getString( cookie, atm_string::id_eject_cash ) );
    action += " | ";
    while ( cookie.getProperty( IAtmCookie::eCashOut, idx++, banknote ) ) {
        action += banknote;
        action += " | ";
    }
    listener.prompt( atm_string::getString( cookie, atm_string::id_remove_cache ), action.c_str() );
    cookie.setProperty( IAtmCookie::eCashOut, NULL );
    return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServicePop::execute( IAtmCookie& cookie,
                             IAtmListener& listener,
                             const char* input,
                             const char*& result ) {
        bool success = false;
        success = execute_extract( cookie, listener );
        if ( success ) {
            success = execute_eject( cookie, listener );
            result = atm_string::getString( cookie, atm_string::id_pop_valid );
        }
        else {
            // Abort immediately
            resetOptions( cookie, listener );
            result = atm_string::getString( cookie, atm_string::id_pop_fail );
        }
        return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServicePop::getName( const IAtmCookie& cookie ) const {
    return atm_string::getString( cookie, atm_string::id_pop_title );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServicePop::getPrompt( const IAtmCookie& cookie ) const {
    return NULL; // no prompt
}


////////////////////////////////////////////////////////////////////////////////////////////////////
int AtmServicePop::getOptionsCount( const IAtmCookie& cookie ) const {
    return 0;
}
 
 
////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServicePop::getOption( const IAtmCookie& cookie,
                                       int index ) const {
    // by predefined value selection -> the requested option
    return NULL;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const int AtmServicePop::getSelectedOption( const IAtmCookie& cookie ) const {
    // ATM service card has no selected options
    return -1;
}

}// namespace atm