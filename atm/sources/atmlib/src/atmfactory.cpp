////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include "atmfactory.h"
#include "atmcookie.h"
#include "atm.h"
#include "atmdumper.h"
#include "atmbank.h"
#include "atmstrategy_asc.h"
#include "atmstrategy_desc.h"
#include "atmstrategy_mix.h"

namespace atm {

namespace {    
    // Unique factory    
    static AtmFactory s_atmFactory;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
IAtmFactory* IAtmFactory::getInstance() {
    // Return the static instance of factory
    return &s_atmFactory;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmFactory::AtmFactory()
: m_atmDumper (NULL) {
    for ( int i = 0 ; i < ATM_COUNT; ++i ) {
        m_atm[ i ] = NULL;
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmFactory::~AtmFactory() {
    delete m_atmDumper;
    for ( int i = 0 ; i < ATM_COUNT; ++i ) {
        delete m_atm[ i ];
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
IAtm* AtmFactory::getAtm( IAtmListener& listener,
                          int index,
                          IAtm::EStrategy strategy ) {
    // Allocate ATM instance, when not exists
    if ( index < ATM_COUNT ) {
        if ( NULL == m_atm[ index ] ) {
            m_atm[ index ] = new Atm( listener, strategy );
        }
        // Return ATM instance
        return m_atm[ index ]; 
    }
    else {
        return NULL;
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
IAtmDumper* AtmFactory::getAtmDumper() {
    if ( NULL == m_atmDumper ) {
        m_atmDumper = new AtmDumper( *AtmBank::getInstance() );
    }
    return m_atmDumper;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
const Atm* AtmFactory::getAtm( const IAtm* atm ) {
    for ( int i= 0 ; i < ATM_COUNT; i++ ) {
        if ( atm == s_atmFactory.m_atm[ i ] ) {
            return s_atmFactory.m_atm[ i ];
        }
    }
    return NULL;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
IAtmCookie* AtmFactory::createCookie( std::map<int, int>& atmStore,
                                      int& minAmount, 
                                      int& maxAmount,
                                      IAtm::EStrategy strategy ) {
    switch( strategy ) {
    case IAtm::eS_Ascendant:
        {
            static AtmStrategyAsc s_asc_strategy;
            return new AtmCookie( atmStore, minAmount, maxAmount, &s_asc_strategy );
        }
        break;
    case IAtm::eS_Descendant:
        {
            static AtmStrategyDesc s_desc_strategy;
            return new AtmCookie( atmStore, minAmount, maxAmount, &s_desc_strategy );
        }
        break;
    default:
        {
            static AtmStrategyMixed s_mix_strategy;
            return new AtmCookie( atmStore, minAmount, maxAmount, &s_mix_strategy );
        }
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmFactory::destroyCookie( IAtmCookie*& cookie ) {
    delete static_cast< AtmCookie* >( cookie );
    cookie = NULL;
}


}// namespace atm
