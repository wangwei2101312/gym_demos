////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atmcookie.h"

namespace atm {

////////////////////////////////////////////////////////////////////////////////////////////////////
AtmCookie::AtmCookie( std::map<int, int>& atmStore,
                      int& minAmount, 
                      int& maxAmount,
                      IAtmStrategy* strategy )
: m_store( atmStore )
, m_maxIn( minAmount )
, m_maxOut( maxAmount )
, m_strategy( strategy ) {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmCookie::~AtmCookie() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmCookie::setProperty( EProperty id, 
                             const char* val ) {
    m_prop[ id ] = ( ( NULL != val ) ? val : "" );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmCookie::addProperty( EProperty id, 
                             const char* val ) {
    if ( m_prop[ id].size() > 0 ) {
        m_prop[ id ] += "\r";
    }
    m_prop[ id ] += val;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmCookie::getProperty( EProperty id ) const {
    return ( ( 0 != *m_prop[id].c_str() ) ? m_prop[id].c_str() : NULL );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmCookie::getProperty( EProperty id,
                             int index,
                             std::string& val ) const {
    int crt = 0;
    std::string::const_iterator it1 = m_prop[ id ].begin();
    std::string::const_iterator it2 = m_prop[ id ].begin();
    for ( ; it2 != m_prop[ id ].end(); it2++ ) {
        if ( ( '\r' == *it2 ) ) {
            if ( crt == index ) {
                val = m_prop[ id ].substr( ( it1 - m_prop[id].begin() ), ( it2 - it1 ) );
                return true;
            }
            it1 = ( ++it2 );
            crt++;
        }
    }
    if ( crt == index ) {
        val = m_prop[ id ].substr( ( it1 - m_prop[id].begin() ) );
        return true;
    }
    return false;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmCookie::getStore( AtmCookie& cookie,
                          std::map<int, int>*& atmStore,
                          int& minAmount, 
                          int& maxAmount ) {
    atmStore = &cookie.m_store;
    minAmount = cookie.m_maxIn;
    maxAmount = cookie.m_maxOut;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
IAtmStrategy* AtmCookie::getStrategy() const {
    return m_strategy;
}

}// namespace atm
