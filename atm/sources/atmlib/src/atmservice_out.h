////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __ATMSERVICE_OUT_H__
#define __ATMSERVICE_OUT_H__

#include "iatmservice.h"

namespace atm {

/// Forward declaration
class IAtmListener;

/** \brief ATM cash retrieval service
  */ 
class AtmServiceOut : public IAtmService
{
public:
    // Visitor method type for bank operating
    typedef bool ( *DispatchBank )( const char*,
                                    const char*,
                                    int );

    // Proxy method for ATM storage access
    typedef bool ( *DispatchAtm )( IAtmCookie&,
                                   int val );
public:
    /** \brief Constructor
      * \param[in] bank - visitor to bank
      * \param[in] atm - ATM storage visitor
      */ 
    AtmServiceOut( DispatchBank bank,
                   DispatchAtm  atm );
    
    /** \brief Virtual destructor
      */ 
    virtual ~AtmServiceOut();
    
public:
    /** \brief Resets the service options stored previously
      * \param[in] cookie - cookie where service stored the selected option
      * \param[in] listener - event message listener
      * \returns selected option value
      */
    virtual void resetOptions( IAtmCookie& cookie, 
                               IAtmListener& listener );

    /** \brief Launches the cash retrieval operation
      * \param[in] cookie - cookie where service stored the selected option
      * \param[in] listener - event message listener
      * \param[in] input - user input
      * \param[out] result - message from service
      * \returns true, when execution succeed, otherwise false
      */ 
    virtual bool execute( IAtmCookie& cookie,
                          IAtmListener& listener,
                          const char*  input,
                          const char*& result );
    
    /** \brief Get service decorative name
      * \param[in] cookie - cookie where service stored the selected option
      * \returns service name string
      */
    virtual const char* getName( const IAtmCookie& cookie ) const;
    
    /** \brief Get service command prompt
      * \param[in] cookie - cookie where service stored the selected option
      * \returns service prompt string
      */
    virtual const char* getPrompt( const IAtmCookie& cookie ) const;

    /** \brief Get service options count
      * \param[in] cookie - cookie where service stored the selected option
      * \returns option count number
      */
    virtual int getOptionsCount( const IAtmCookie& cookie ) const;
    
    /** \brief Get service options string
      * \param[in] cookie - cookie where service stored the selected option
      * \param[in] index - option index
      * \returns option text
      */
    virtual const char* getOption( const IAtmCookie& cookie,
                                   int index ) const;
    
    /** \brief Get selected option by the service, used to determine the next state handler service
      * \param[in] cookie - cookie where service stored the selected option
      * \returns selected option value
      */
    virtual const int getSelectedOption( const IAtmCookie& cookie ) const;

protected:
    /** \brief Launches the cash retrieval operation with selection
      * \param[in] cookie - cookie where service stored the selected option
      * \param[in] listener - event message listener
      * \param[in] input - user input
      * \param[out] result - message from service
      * \returns true, when execution succeed, otherwise false
      */ 
    virtual bool execute_selection( IAtmCookie& cookie,
                                    IAtmListener& listener,
                                    const char*  input,
                                    const char*& result );

     /** \brief Launches the cash retrieval operation with manual type-in
      * \param[in] cookie - cookie where service stored the selected option
      * \param[in] listener - event message listener
      * \param[in] input - user input
      * \param[out] result - message from service
      * \returns true, when execution succeed, otherwise false
      */ 
    virtual bool execute_manual( IAtmCookie& cookie,
                                 IAtmListener& listener,
                                 const char*  input,
                                 const char*& result );

     /** \brief Transaction dispatch
      * \param[in] cookie - cookie where service stored the selected option
      * \param[in] listener - event message listener
      * \param[out] result - message from service
      * \returns true, when execution succeed, otherwise false
      */ 
    virtual bool execute_dispatch( IAtmCookie& cookie,
                                   IAtmListener& listener,
                                   const char*& result );
private:
    // Disable auto-generated default/copy constructor and assignment operator
    AtmServiceOut();
    AtmServiceOut( const AtmServiceOut& );
    AtmServiceOut& operator =( const AtmServiceOut& );

protected:
    // Dispatcher to bank
    DispatchBank m_bank;
    // Dispatcher to ATM storage
    DispatchAtm  m_atm;
};

}// namespace atm

#endif// __ATMSERVICE_OUT_H__
