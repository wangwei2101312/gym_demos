////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __IATMCOOKIE_H__
#define __IATMCOOKIE_H__

#include <string>

namespace atm {

/// Forward declarations
class IAtmStrategy;

/** \brief ATM state machine interface class
  */ 
class IAtmCookie
{
public:
    /** \brief Properties stored by cookie
      */ 
    enum EProperty {
        eLanguage = 0,
        eCardId,
        ePin,
        eOption,
        eAmountType,
        eAmountVal,
        eCashIn,
        eCashOut,
        eTicket,
        eLast
    };

protected:
    /** \brief Virtual destructor
      */ 
    virtual ~IAtmCookie() {}
    
public:
    /** \brief Set property for cookie
      * \param[in] id - property identifier (see EProperty)
      * \param[in] val - property value
      */ 
    virtual void setProperty( EProperty id, 
                              const char* val ) = 0;

    /** \brief Append value to cookie property
      * \param[in] id - property identifier (see EProperty)
      * \param[in] val - property value
      */ 
    virtual void addProperty( EProperty id, 
                              const char* val ) = 0;

    /** \brief Get property from cookie
      * \param[in] id - property identifier (see EProperty)
      * \returns property value
      */ 
    virtual const char* getProperty( EProperty id ) const = 0;

    /** \brief Get a property from cookie property array
      * \param[in] id - property identifier (see EProperty)
      * \param[in] index - index of property array
      * \param[out] val - string where to return
      * \returns true, when succeed, otherwise false
      */ 
    virtual bool getProperty( EProperty id, 
                              int index,
                              std::string& val ) const = 0;

    /** \brief Get ATM operating strategy
      * \returns strategy interface
      */ 
    virtual IAtmStrategy* getStrategy() const = 0;
};

}// namespace atm

#endif// __IATMCOOKIE_H__
