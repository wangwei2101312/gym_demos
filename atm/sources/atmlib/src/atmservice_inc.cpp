////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atmservice_inc.h"
#include "iatmcookie.h"
#include "atmstrings.h"
#include <stdlib.h>
#include <string.h>

namespace atm {

namespace {
    // Card fill-up service options count
    const int   s_option_count    = 6;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServiceInc::AtmServiceInc( DispatchBank bank,
                               DispatchAtm  atm )
: m_bank( bank )
, m_atm( atm ) {    
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServiceInc::~AtmServiceInc() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmServiceInc::resetOptions( IAtmCookie& cookie,
                                  IAtmListener& listener ) {
    cookie.setProperty( IAtmCookie::eAmountType, NULL );
    cookie.setProperty( IAtmCookie::eAmountVal, NULL );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServiceInc::execute_dispatch( IAtmCookie& cookie,
                                      IAtmListener& listener,
                                      const char*& result ) {
    bool success = false;
    if ( NULL != cookie.getProperty( IAtmCookie::eAmountVal ) ) {
        if ( ( *m_atm )( cookie, atoi( cookie.getProperty( IAtmCookie::eAmountVal ) ) ) &&
             ( *m_bank )( cookie.getProperty( IAtmCookie::eCardId ), 
                         cookie.getProperty( IAtmCookie::ePin ),
                         atoi( cookie.getProperty( IAtmCookie::eAmountVal ) ) ) ) {
                result = atm_string::getString( cookie, atm_string::id_inc_valid_1 );
                success = true;
        } 
        else {
            result = atm_string::getString( cookie, atm_string::id_inc_rejected );
        }
    }
    else {
        result = atm_string::getString( cookie, atm_string::id_inc_invalid );
    }
    return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServiceInc::execute_manual( IAtmCookie& cookie,
                                    IAtmListener& listener,
                                    const char* input,
                                    const char*& result ) {
    // Manual type-in of amount
    bool success = false;
    if ( ( NULL != input ) && ( atoi(input) > 0 ) ) {
        cookie.setProperty( IAtmCookie::eAmountVal, input );
        success = execute_dispatch( cookie, listener, result );
    }
    else {
        result = atm_string::getString( cookie, atm_string::id_inc_invalid );
    }
    return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServiceInc::execute_selection( IAtmCookie& cookie,
                                       IAtmListener& listener,
                                       const char* input,
                                       const char*& result ) {
    // Predefined selection of amount
    bool success = false;
    bool nextSubState = false;
    if ( ( NULL != input ) && ( strlen(input) == 1 ) ) {
        switch( *input ) {
        case '1':
            cookie.setProperty( IAtmCookie::eAmountVal, "50" );
            success = true;
            break;
        case '2':
            cookie.setProperty( IAtmCookie::eAmountVal, "100" );
            success = true;
            break;
        case '3':
            cookie.setProperty( IAtmCookie::eAmountVal, "200" );
            success = true;
            break;
        case '4':
            cookie.setProperty( IAtmCookie::eAmountVal, "500" );
            success = true;
            break;
        case '5':
            cookie.setProperty( IAtmCookie::eAmountVal, "1000" );
            success = true;
            break;
        case '6':
            cookie.setProperty( IAtmCookie::eAmountType, "1" );
            result = atm_string::getString( cookie, atm_string::id_inc_valid_2 );
            success = true;
            nextSubState = true;
            break;
        }
    }
    if ( success ) {
        if ( !nextSubState ) {
            success = execute_dispatch( cookie, listener, result );
        }
    }
    else {
        result = atm_string::getString( cookie, atm_string::id_inc_invalid_sel );
    }
    return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServiceInc::execute( IAtmCookie& cookie,
                             IAtmListener& listener,
                             const char* input,
                             const char*& result ) {
    // Verify selected option
    bool success = false;    
    // Determine sub-state
    int amountType = (   ( NULL != cookie.getProperty( IAtmCookie::eAmountType ) ) 
                       ? ( *cookie.getProperty( IAtmCookie::eAmountType ) - '0' ) 
                       : 0 );
    // Reset previous selections - by any failure the sub-state machine goes to the first state
    cookie.setProperty( IAtmCookie::eAmountType, NULL );
    cookie.setProperty( IAtmCookie::eAmountVal, NULL );
    // Execute sub-state
    if ( 1 == amountType ) {
        success = execute_manual(cookie, listener, input, result );
    }
    else {
        success = execute_selection(cookie, listener, input, result );
    }
    return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServiceInc::getName( const IAtmCookie& cookie ) const {
    return atm_string::getString( cookie, atm_string::id_inc_title );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServiceInc::getPrompt( const IAtmCookie& cookie ) const {
    // Determine sub-state
    int amountType = (   ( NULL != cookie.getProperty( IAtmCookie::eAmountType ) ) 
                       ? ( *cookie.getProperty( IAtmCookie::eAmountType ) - '0' ) 
                       : 0 );
    if ( 1 == amountType ) {
        // by manual type-in
        return atm_string::getString( cookie, atm_string::id_inc_prompt_2 );
    }
    else {
        return atm_string::getString( cookie, atm_string::id_inc_prompt_1 );
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
int AtmServiceInc::getOptionsCount( const IAtmCookie& cookie ) const {
    // Determine sub-state
    int amountType = (   ( NULL != cookie.getProperty( IAtmCookie::eAmountType ) ) 
                       ? ( *cookie.getProperty( IAtmCookie::eAmountType ) - '0' ) 
                       : 0 );
    if ( 1 == amountType ) {
        // by manual type-in -> no options
        return 0;
    }
    else {
        // by predefined value selection -> the options count
        return s_option_count;
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServiceInc::getOption( const IAtmCookie& cookie,
                                      int index ) const {
    // Determine sub-state
    int amountType = (   ( NULL != cookie.getProperty( IAtmCookie::eAmountType ) ) 
                       ? ( *cookie.getProperty( IAtmCookie::eAmountType ) - '0' )
                       : 0 );
    if ( 1 == amountType ) {
        // by manual type-in -> no options
        return NULL;
    }
    else {
        // by predefined value selection -> the requested option
        if (  index < s_option_count ) {
            const char* strOpt = atm_string::getString( cookie, atm_string::id_inc_options );
            for (int opt = 0; opt < index; opt++ ) {
                strOpt += ( strlen( strOpt ) + 1 );
            }
            return strOpt;
        }
        return NULL;
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const int AtmServiceInc::getSelectedOption( const IAtmCookie& cookie ) const {
    // ATM service inc selected option
    return (    ( NULL != cookie.getProperty( IAtmCookie::eOption ) ) 
        ?  ( *cookie.getProperty( IAtmCookie::eOption ) - '0' ) 
        : -1 );
}


}// namespace atm