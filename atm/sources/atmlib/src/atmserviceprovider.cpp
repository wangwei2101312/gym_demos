////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include "atmbank.h"
#include "atmcookie.h"
#include "atmserviceprovider.h"
#include "atmservice_lng.h"
#include "atmservice_card.h"
#include "atmservice_pin.h"
#include "atmservice_opt.h"
#include "atmservice_inc.h"
#include "atmservice_out.h"
#include "atmservice_push.h"
#include "atmservice_pop.h"
#include "atmservice_prn.h"
#include "iatmstrategy.h"

namespace atm {

////////////////////////////////////////////////////////////////////////////////////////////////////
namespace {

    /** \brief Dispatcher class to visit bank and ATM protected services
      */ 
    class AtmDispatcher {
    public:
        /// ATM visitor method for card reading
        static bool bankReadCard( const char* card ) {
            return AtmBank::getInstance()->checkCardId( card );
        }

        /// ATM visitor method for pin code verifying
        static bool bankCheckPin( const char* card,
                                  const char* pin ) {
            return AtmBank::getInstance()->checkPin( card, pin );
        }

        /// ATM visitor method for card fill-up test
        static bool bankCheckIn( const char* card,
                                  const char* pin,
                                  int val ) {
            return AtmBank::getInstance()->extract( card, pin, -val, true );
        }

        /// ATM visitor method for cash retrieval test
        static bool bankCheckOut( const char* card,
                                  const char* pin,
                                  int val ) {
            return AtmBank::getInstance()->extract( card, pin, val, true );
        }

        /// ATM visitor method for card fill-up test
        static bool bankPush( const char* card,
                              const char* pin,
                              int val ) {
            return AtmBank::getInstance()->extract( card, pin, -val, false );
        }

        /// ATM visitor method for cash retrieval test
        static bool bankPop( const char* card,
                             const char* pin,
                             int val ) {
            return AtmBank::getInstance()->extract( card, pin, val, false );
        }

        /// ATM visitor method for cash retrieval test
        static bool bankInfo( const char* card,
                              const char* pin,
                              char type,
                              std::vector< int >& info ) {
                return AtmBank::getInstance()->showInfo( card, pin, type, info );
        }

        /// ATM visitor method for cash insertion
        static int atmPush( IAtmCookie& cookie, 
                             int val ) {
            int minVal;
            int maxVal;
            std::map<int, int >* store;
            AtmCookie::getStore( reinterpret_cast<AtmCookie&>( cookie ), store, minVal, maxVal );
            if ( val >= 0 ) {
                // Extract a bank-note into the locked space
                std::map<int, int >::reverse_iterator it;
                for ( it = store->rbegin(); it != store->rend(); ++it ) {
                    if ( ( it->second > 0 ) && ( it->first == val  ) ) {
                        // Insert
                        it->second++;
                        return it->first;
                    }
                }
                return 0;
            }
            else {
                // Restore a banknote from ATM
                store->find( -val )->second--;
                return val;
            }
        }

        /// ATM visitor method for cash insertion limits
        static bool atmPushLimits( IAtmCookie& cookie, 
                                   int val ) {
            int minVal;
            int maxVal;
            std::map<int, int >* store;
            AtmCookie::getStore( reinterpret_cast<AtmCookie&>( cookie ), store, minVal, maxVal );
            return ( val <= maxVal );
        }

        /// ATM visitor method for cash payment
        static int atmPop( IAtmCookie& cookie, 
                           int val ) {
            int minVal;
            int maxVal;
            std::map<int, int >* store;
            AtmCookie::getStore( reinterpret_cast<AtmCookie&>( cookie ), store, minVal, maxVal );
            if ( val >= 0 ) {
                // Extract a bank-note
                return cookie.getStrategy()->extract( *store, val );
            }
            else {
                // Restore a banknote to ATM
                store->find( -val )->second++;
                return val;
            }
        }

        /// ATM visitor method for retrieval limits
        static bool atmPopLimits( IAtmCookie& cookie, 
                                  int val ) {
            int minVal;
            int maxVal;
            std::map<int, int >* store;
            AtmCookie::getStore( reinterpret_cast<AtmCookie&>( cookie ), store, minVal, maxVal );
            if ( ( val <= maxVal ) && (val >= ( minVal ) ) ) {
                // Create a copy for simulation
                std::map<int, int > fakeStore( *store );
                // Extract a bank-note
                int banknote = 0;
                do {
                    banknote = cookie.getStrategy()->extract( fakeStore, val );
                    val -= banknote;
                } while ( ( banknote > 0 ) && ( val > 0 ) );
                return ( val == 0 );
            }
            else {
                return false;
            }
        }

    };


    /// ATM services
    /// ATM language selector
    static AtmServiceLng       s_lngService;
    /// ATM card reader service 
    static AtmServiceCard      s_cardService( &AtmDispatcher::bankReadCard );
    /// ATM pin checker service 
    static AtmServicePin       s_pinService( &AtmDispatcher::bankCheckPin );
    /// ATM option service 
    static AtmServiceOpt       s_optService;
    /// ATM card fill-up service 
    static AtmServiceInc       s_incService( &AtmDispatcher::bankCheckIn, &AtmDispatcher::atmPushLimits );
    /// ATM cash retrieval service 
    static AtmServiceOut       s_outService( &AtmDispatcher::bankCheckOut, &AtmDispatcher::atmPopLimits );
    /// ATM local storage handler for inputs
    static AtmServicePush      s_pushService( &AtmDispatcher::bankPush, &AtmDispatcher::atmPush );
    /// ATM local storage handler for outputs
    static AtmServicePop       s_popService( &AtmDispatcher::bankPop, &AtmDispatcher::atmPop );
    /// ATM printer handler
    static AtmServicePrn       s_prnService( *AtmDispatcher::bankInfo );
    
    /// Services structure
    struct Services {
        IAtmService* current;
        IAtmService* next;
        int          option;
        bool         reset;   // reset service option by switching to
        bool         fatal;   // fatal error, when service fails (ATM session need to be canceled)
    };

    /// State machine for services in forward direction
    const Services states_next[] = {
        { NULL,               &s_lngService,     -1, true,  false }, // from start go to card reader    
        { &s_lngService,      &s_cardService,    -1, true,  false }, // from start go to card reader    
        { &s_cardService,     &s_pinService,     -1, true,  true }, // from card reader go to pin checker
        { &s_pinService,      &s_optService,     -1, true,  false }, // from pin checker go to transaction type
        { &s_optService,      &s_outService,      1, true,  false }, // from transaction type go to cash retrieval
        { &s_optService,      &s_incService,      2, true,  false }, // from transaction type go to card fill-up
        { &s_optService,      &s_prnService,      3, true,  false }, // from transaction type go to account info
        { &s_optService,      &s_optService,     -1, true,  false }, // from transaction type go to nowhere wen no valid option
        { &s_incService,      &s_incService,      1, false, false }, // next sub-state of card fill-up service
        { &s_outService,      &s_outService,      1, false, false }, // next sub-state of cash retrieval service
        { &s_incService,      &s_pushService,    -1, true,  false }, // next to cash retrieval go to local service to operate
        { &s_outService,      &s_popService,     -1, true,  false }, // next to cash retrieval go to local service to operate
        { &s_pushService,     &s_pushService,     1, false, false }, // next to local service go to printer, when user wants
        { &s_pushService,     &s_prnService,      2, true,  true  }, // next to local service go to printer, when user wants
        { &s_popService,      &s_prnService,      2, true,  true  }, // next to local service go to printer, when user wants
        { &s_pushService,     NULL,              -1, false, true  }, // next to local service go to exit
        { &s_popService,      NULL,              -1, false, true  }, // next to local service go to exit
    };

    /// State machine for services in backward direction
    const Services states_prev[] = {
        { NULL,            NULL,           -1, false, false },     
        { &s_lngService,   NULL,           -1, false, false },     
        { &s_cardService,  NULL,           -1, false, false },     
        { &s_pinService,   NULL,           -1, false, false },     
        { &s_optService,   NULL,           -1, false, false },
        { &s_outService,   &s_optService,  -1, false, false },
        { &s_incService,   &s_optService,  -1, false, false },
        { &s_prnService,   NULL,           -1, false, false },
        { &s_pushService,  NULL,           -1, false, false },
        { &s_popService,   NULL,           -1, false, false },
    };
    
    /// Schedule for services exiting
    const Services states_exit[] = {
        { &s_popService   },
        { &s_pushService  },
        { &s_cardService  },
        { &s_prnService   },
    };
}


////////////////////////////////////////////////////////////////////////////////////////////////////
IAtmService* AtmServiceProvider::getNextService( IAtmService* service,
                                                 IAtmCookie& cookie,
                                                 IAtmListener& listener,
                                                 bool result ) {
    int option = ( ( NULL != service ) ? service->getSelectedOption( cookie ) : -1 );
    for ( int s = 0; s < ( sizeof( states_next ) / sizeof( states_next[ 0 ] ) ); s++ ) {
        // Matching the next state handler service for ATM based on:
        // - current service
        // - selected option of current service, -1 means doesn't matter the selected option
        if ( ( service == states_next[ s ].current ) && 
             ( ( option == states_next[ s ].option ) || ( -1 == states_next[ s ].option ) ) ) {     
            // Matched the current state/ next pair
            // Check if service failed and it means fatal error
            if ( !result ) {
                // When not fatal - stay by the same service, otherwise end session
                return ( states_next[ s ].fatal ? NULL : service );
            }
            // Check if option reset is needed
            if ( states_next[ s ].reset ) {
                states_next[ s ].next->resetOptions( cookie, listener );
            }
            return states_next[ s ].next;
        }
    }
    return NULL;
}
      

////////////////////////////////////////////////////////////////////////////////////////////////////
IAtmService* AtmServiceProvider::getPrevService( IAtmService* service,
                                                 IAtmCookie& cookie,
                                                 IAtmListener& listener ) {
    int option = ( ( NULL != service ) ? service->getSelectedOption( cookie ) : -1 );
    for ( int s = 0; s < ( sizeof( states_prev ) / sizeof( states_prev[ 0 ] ) ); s++ ) {
        // Matching the previous state handler service for ATM based on:
        // - current service
        // - selected option of current service, -1 means doesn't matter the selected option
        if ( ( service == states_prev[ s ].current ) && 
             ( ( option == states_prev[ s ].option ) || ( -1 == states_prev[ s ].option ) ) ) {
            if ( states_prev[ s ].reset ) {
                states_prev[ s ].next->resetOptions( cookie, listener );
            }
            return states_prev[ s ].next;
        }
    }
    return NULL;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmServiceProvider::exitServices( IAtmCookie& cookie,
                                       IAtmListener& listener ) {
    // Iterate services subscribed for exiting
    for ( int s = 0; s < ( sizeof( states_exit ) / sizeof( states_exit[ 0 ] ) ); s++ ) {
        states_exit[ s ].current->resetOptions( cookie, listener );
    }
}

}// namespace atm 