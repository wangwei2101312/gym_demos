////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __ATM_SERVICEPROVIDER_H__
#define __ATM_SERVICEPROVIDER_H__

#include <iatmfactory.h>

namespace atm {
    
/// Forward declarations
class IAtmService;    
class IAtmCookie;    


/** \brief ATM factory class
  *  Implements an ATM factory interface
  */ 
class AtmServiceProvider {
public:
    /** \brief Provides the next ATM state handler service
      * \param[in] service - currently active ATM service
      * \param[in] cookie - cookie, where service stored the selected option
      * \param[in] listener - event message listener
      * \param[in] result - of current service execution
      * \return pointer to the next ATM service instance, when succeed, otherwise NULL
      */ 
    static IAtmService* getNextService( IAtmService* service, 
                                        IAtmCookie& cookie,
                                        IAtmListener& listener,
                                        bool result );
      
    /** \brief Provides the previous ATM state handler service
      * \param[in] service - currently active ATM service
      * \param[in] cookie - cookie, where service stored the selected option
      * \param[in] listener - event message listener
      * \return pointer to the previous ATM service instance, when succeed, otherwise NULL
      */ 
    static IAtmService* getPrevService( IAtmService* service,
                                        IAtmCookie& cookie ,
                                        IAtmListener& listener );

    /** \brief Sends the exit signal for all services
      * \param[in] cookie - cookie, where service stored the selected option
      * \param[in] listener - event message listener
      */ 
    static void exitServices( IAtmCookie& cookie,
                              IAtmListener& listener );


private:
    // Disable auto-generated default/copy constructor and assignment operator
    AtmServiceProvider();
    AtmServiceProvider( const AtmServiceProvider& );
    AtmServiceProvider& operator =( const AtmServiceProvider& );
    
};

}// namespace atm

#endif// ATM_SERVICEPROVIDER_H
