////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atmservice_opt.h"
#include "iatmcookie.h"
#include "atmstrings.h"
#include <stdlib.h>
#include <string.h>

namespace atm {

namespace {
    // const int   s_option_count    = 3; switch back to here, when printer service is functional
    const int   s_option_count    = 2;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServiceOpt::AtmServiceOpt() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServiceOpt::~AtmServiceOpt() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmServiceOpt::resetOptions( IAtmCookie& cookie,
                                  IAtmListener& listener ) {
    cookie.setProperty( IAtmCookie::eOption, NULL );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServiceOpt::execute( IAtmCookie& cookie,
                             IAtmListener& listener,
                             const char* input,
                             const char*& result ) {
    // Verify selected option
    bool success = false;
    result = atm_string::getString( cookie, atm_string::id_opt_invalid );
    if ( ( NULL != input ) && ( strlen(input) == 1 ) ) {
        switch( *input ) {
            case '1':
            case '2':
            case '3':
                cookie.setProperty( IAtmCookie::eOption, input );
                result = atm_string::getString( cookie, atm_string::id_opt_valid );
                success = true;
                break;
        }
    }
    return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServiceOpt::getName( const IAtmCookie& cookie ) const {
    return atm_string::getString( cookie, atm_string::id_opt_title );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServiceOpt::getPrompt( const IAtmCookie& cookie ) const {
    return atm_string::getString( cookie, atm_string::id_opt_prompt );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
int AtmServiceOpt::getOptionsCount( const IAtmCookie& cookie ) const {
    return s_option_count;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServiceOpt::getOption( const IAtmCookie& cookie,
                                      int index ) const {
    if (  index < s_option_count ) {
        const char* strOpt = atm_string::getString( cookie, atm_string::id_opt_options );
        for (int opt = 0; opt < index; opt++ ) {
            strOpt += ( strlen( strOpt ) + 1 );
        }
        return strOpt;
    }
    return NULL;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const int AtmServiceOpt::getSelectedOption( const IAtmCookie& cookie ) const {
    // ATM service opt has selected options
    return (    ( NULL != cookie.getProperty( IAtmCookie::eOption ) ) 
             ?  ( *cookie.getProperty( IAtmCookie::eOption ) - '0' ) 
             : -1 );
}


}// namespace atm