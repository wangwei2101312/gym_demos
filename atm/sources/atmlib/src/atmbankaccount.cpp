////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atmbankaccount.h"
#include <iatmdumplistener.h>
#include <stdio.h>

namespace atm {

////////////////////////////////////////////////////////////////////////////////////////////////////
AtmBankAccount::AtmBankAccount( const char* owner, 
                                int balance,
                                int debit,
                                int credit ) 
: m_owner( owner )
, m_ins()
, m_outs()
, m_maxDebit( debit )
, m_maxCredit( credit ) {
    m_ins.push_back( balance );
}
    

////////////////////////////////////////////////////////////////////////////////////////////////////
AtmBankAccount::AtmBankAccount( const AtmBankAccount& other ) {
    /// Owner of account
    m_owner = other.m_owner;
    m_ins = other.m_ins;
    m_outs = other.m_outs;
    m_maxDebit = other.m_maxDebit;
    m_maxCredit = other.m_maxCredit;

}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmBankAccount::~AtmBankAccount() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmBankAccount::extract( int value, 
                              bool simulate ) {
    if ( 0 != value ) {
        // Valid movements only
        std::list<int>::iterator it;
        int balance = 0;
        for ( it = m_ins.begin(); it != m_ins.end(); ++it ) {
            balance += *it;
        }
        for ( it = m_outs.begin(); it != m_outs.end(); ++it ) {
            balance -= *it;
        }
        int candidate = ( balance  - value );
        if ( ( candidate <= m_maxDebit ) &&
             ( ( candidate + m_maxCredit ) >= 0 ) ) {
            if ( !simulate ) {
                // Apply the operation
                if ( value > 0 ) {
                    m_outs.push_back( value );
                }
                else {
                    m_ins.push_back( -value );
                }
            }
            return true;
        }
    }
    return false;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmBankAccount::showInfo( std::vector<int >& info,
                               char type ) {
    std::list<int>::const_iterator it;
    info.push_back( getBalance() );
    if ( 1 == type ) {
        info.push_back( getCredit() );
    }
    else if ( 2 == type ) {
        for ( it = m_ins.begin(); it != m_ins.end(); ++it ) {
            info.push_back( *it );
        }
        for ( it = m_outs.begin(); it != m_outs.end(); ++it ) {
            info.push_back( -( *it ) );
        }
    }
    return true;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
int AtmBankAccount::getBalance() const {
    std::list<int>::const_iterator it;
    int balance = 0;
    for ( it = m_ins.begin(); it != m_ins.end(); ++it ) {
        balance += *it;
    }
    for ( it = m_outs.begin(); it != m_outs.end(); ++it ) {
        balance -= *it;
    }
    return balance;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
int AtmBankAccount::getDebit() const {
    return m_maxDebit;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
int AtmBankAccount::getCredit() const {
    return m_maxCredit;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmBankAccount::getOwner() const {
    return m_owner.c_str();
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmBankAccount::dump( IAtmDumpListener& listener,
                           int level ) const {
    char val[64];
    std::string strval;
    std::list<int>::const_iterator it;

    listener.dump( "Owner", m_owner.c_str(), level );

    sprintf( val, "%d", getBalance() );
    listener.dump( "Balance", val, level );

    sprintf( val, "%d", getCredit() );
    listener.dump( "Max. credit", val, level );

    strval = "";
    for ( it = m_ins.begin(); it != m_ins.end(); ++it ) {
        sprintf( val, "<%d>", *it );
        strval += val;
    }
    listener.dump( "Incomes", strval.c_str(), level );

    strval = "";
    for ( it = m_outs.begin(); it != m_outs.end(); ++it ) {
        sprintf( val, "<%d>", *it );
        strval += val;
    }
    listener.dump( "Payments", strval.c_str(), level );
}

}// namespace atm

