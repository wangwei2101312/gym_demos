////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __ATMSTRINGS_H__
#define __ATMSTRINGS_H__

namespace atm {

/// Forward declarations
class IAtmCookie;

namespace atm_string {

    /** \brief ATM string identifiers
      */ 
    enum EStringId {
        id_none = 0,
        id_card_title,
        id_card_prompt,
        id_card_invalid,
        id_card_rejected,
        id_card_loaded,
        id_remove_cache,
        id_remove_card,
        id_remove_ticket,
        id_eject_card,
        id_eject_cash,
        id_eject_ticket,
        id_dsp_title,
        id_pin_title, 
        id_pin_prompt,
        id_pin_invalid,
        id_pin_valid,
        id_pin_rejected,
        id_inc_title,
        id_inc_prompt_1,
        id_inc_prompt_2,
        id_inc_options,
        id_inc_invalid,
        id_inc_valid_1,
        id_inc_valid_2,
        id_opt_title,
        id_opt_prompt,
        id_opt_invalid,
        id_opt_valid,
        id_opt_options,
        id_out_title,
        id_out_prompt_1,
        id_out_prompt_2,
        id_out_options,
        id_out_invalid,
        id_out_valid_1,
        id_out_valid_2,
        id_inc_rejected,
        id_out_rejected,
        id_inc_invalid_sel,
        id_out_invalid_sel,
        id_push_title,
        id_pop_title,
        id_push_prompt,
        id_push_remained,
        id_push_invalid,
        id_push_fail,
        id_pop_fail,
        id_push_valid,
        id_pop_valid,
        id_prn_prompt,
        id_prn_options,
        id_abort_cash,
        id_cancel_cash,
        id_push_banknote_ok,
        id_prn_title_0,
        id_prn_prompt_0,
        id_prn_options_0,
    };


    /** \brief ATM string retrieval, based on cookie provided language
      * \param[in] cookie - cookie for language retrieval
      * \param[in] stringId - unique string identifier
      * \return - localized string 
      */ 
    const char* getString( const IAtmCookie& cookie, 
                           EStringId stringId );


}// namespace atm_string

}// namespace atm

#endif// __ATMSERVIVE_RMT_H__
