////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atmservice_prn.h"
#include "iatmcookie.h"
#include "atmstrings.h"
#include <stdlib.h>
#include <string.h>

namespace atm {


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServicePrn::AtmServicePrn( DispatchBank bank )
: m_bank( bank ) {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServicePrn::~AtmServicePrn() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmServicePrn::resetOptions( IAtmCookie& cookie,
                                  IAtmListener& listener ) {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServicePrn::execute( IAtmCookie& cookie,
                             IAtmListener& listener,   
                             const char* input,
                             const char*& result ) {
    return true;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServicePrn::getName( const IAtmCookie& cookie ) const {
    return atm_string::getString( cookie, atm_string::id_prn_title_0 );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServicePrn::getPrompt( const IAtmCookie& cookie ) const {
    if ( NULL == cookie.getProperty( IAtmCookie::eTicket ) ) {
        return atm_string::getString( cookie, atm_string::id_prn_prompt_0 );
    }
    else {
        return atm_string::getString( cookie, atm_string::id_prn_prompt );
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
int AtmServicePrn::getOptionsCount( const IAtmCookie& cookie ) const {
    return 0;
}
 
 
////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServicePrn::getOption( const IAtmCookie& cookie,
                                       int index ) const {
    return NULL;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const int AtmServicePrn::getSelectedOption( const IAtmCookie& cookie ) const {
    // ATM service card has no selected options
    return -1;
}

}// namespace atm