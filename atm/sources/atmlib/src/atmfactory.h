////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __ATM_FACTORY_H__
#define __ATM_FACTORY_H__

#include <iatmfactory.h>
#include <map>

namespace atm {
    
/// Forward declarations
class Atm;
class IAtmCookie;
class AtmDumper;


/** \brief ATM factory class
  *  Implements an ATM factory interface
  */ 
class AtmFactory : public IAtmFactory {
public:
    /** \brief Constructor
      */ 
    AtmFactory();
    
    /** \brief Virtual destructor
      */ 
    virtual ~AtmFactory();
    
public:        
    /** \brief Provides access to an ATM instance
      * \param[in] listener - ATM listener object
      * \param[in] index - ATM index
      * \param[in] strategy - strategy to operate ATM store
      * \return pointer to a valid ATM instance, when succeed, otherwise NULL
      */ 
    virtual IAtm* getAtm( IAtmListener& listener,
                          int index,
                          IAtm::EStrategy strategy );

    /** \brief Provides access to an ATM dumper
      * \return pointer to a valid ATM instance, when succeed, otherwise NULL
      */ 
    virtual IAtmDumper* getAtmDumper();

public:
    /** \brief Provides access to an ATM instance
      * \param[in] atm - ATM interface
      * \return pointer to a valid ATM instance, when succeed, otherwise NULL
      */ 
    static const Atm* getAtm( const IAtm* atm );

    /** \brief Creates an ATM cookie
      * \param[in] atmStore - ATM store
      * \param[in] minAmount - minimum amount to operate
      * \param[in] maxAmount - maximum amount to operate
      * \param[in] strategy - strategy to operate ATM store
      * \return pointer to a valid ATM cookie instance, when succeed, otherwise NULL
      */ 
    static IAtmCookie* createCookie( std::map<int, int>& atmStore,
                                     int& minAmount, 
                                     int& maxAmount,
                                     IAtm::EStrategy strategy );

    /** \brief Destroys an ATM cookie and resets the pointer
      * \param[in/out] cookie - pointer to ATM cookie
      */ 
    static void destroyCookie( IAtmCookie*& cookie );

protected:
    static const int ATM_COUNT = 3;
    // Pointer to ATM dumper instance
    AtmDumper*      m_atmDumper;		
	// Pointers to ATM instances
	Atm*            m_atm[ ATM_COUNT ];		
    
private:
    // Disable auto-generated copy constructor and assignment operator
    AtmFactory( const AtmFactory& );
    AtmFactory& operator =( const AtmFactory& );
    
};

}// namespace atm

#endif// ATM_FACTORY_H
