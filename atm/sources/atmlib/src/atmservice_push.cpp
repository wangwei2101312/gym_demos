////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atmservice_push.h"
#include "iatmcookie.h"
#include "atmstrings.h"
#include "iatmlistener.h"
#include <stdlib.h>
#include <string>
#include <string.h>
#include <stdio.h>

namespace atm {


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServicePush::AtmServicePush( DispatchBank bank,
                                DispatchAtm  atm )
: m_bank( bank )
, m_atm( atm ) {    
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServicePush::~AtmServicePush() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmServicePush::resetOptions( IAtmCookie& cookie,
                                  IAtmListener& listener) {
    if ( NULL != cookie.getProperty( IAtmCookie::eCashIn ) ) {
        int idx = 0;
        std::string banknote;
        std::string action( atm_string::getString( cookie, atm_string::id_eject_cash ) );
        while ( cookie.getProperty( IAtmCookie::eCashIn, idx++, banknote ) ) {
            // Restore to ATM storage
            ( *m_atm )( cookie, -atoi( banknote.c_str() ) );
            action += banknote;
            action += " | ";
        }
        listener.prompt( atm_string::getString( cookie, atm_string::id_remove_cache ), action.c_str() );
        cookie.setProperty( IAtmCookie::eCashIn, NULL );
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServicePush::execute_transaction( IAtmCookie& cookie,
                                          IAtmListener& listener,
                                          const char*& result ) {
    bool success = false;
    if ( ( *m_bank )( cookie.getProperty( IAtmCookie::eCardId ), 
                      cookie.getProperty( IAtmCookie::ePin ),
                      getCash( cookie ) ) ) {   
        cookie.setProperty( IAtmCookie::eCashIn, NULL );
        result = atm_string::getString( cookie, atm_string::id_push_valid );
        success = true;
    } 
    else {
        // Abort immediately
        resetOptions( cookie, listener );
        result = atm_string::getString( cookie, atm_string::id_push_fail );
    }
    return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServicePush::execute_insertion( IAtmCookie& cookie,
                                        IAtmListener& listener,
                                        const char* input,
                                        const char*& result ) {
    bool success = false;
    int remained = ( getAmount( cookie ) - getCash( cookie ) );
    if ( ( NULL != input ) && ( atoi( input) > 0 ) && ( atoi( input) <= remained ) ) {
        // Access ATM store
        if ( ( *m_atm )( cookie, atoi( input ) ) ) {
            cookie.addProperty( IAtmCookie::eCashIn, input );
            remained = ( getAmount( cookie ) - getCash( cookie ) );
            if ( 0 == remained ) {
                success = execute_transaction( cookie, listener, result );
            }
            else {
                result = atm_string::getString( cookie, atm_string::id_push_banknote_ok );
                success = true;
            }
        }
        else {
            result = atm_string::getString( cookie, atm_string::id_push_invalid );
        }
    }
    else {
        result = atm_string::getString( cookie, atm_string::id_push_invalid );
    }

    char buff[64 ];
    sprintf(buff, "%d", remained );
    listener.prompt( atm_string::getString( cookie, atm_string::id_push_remained ), buff );

    return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServicePush::execute( IAtmCookie& cookie,
                             IAtmListener& listener,
                             const char* input,
                             const char*& result ) {
    bool success = false;
    int subState = ( getCash( cookie ) < getAmount( cookie ) ? 1 : 0 );
    if ( 1 == subState ) {
        success = execute_insertion( cookie, listener, input, result );
    }
    else {
        success = execute_transaction( cookie, listener, result );
    }
    return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServicePush::getName( const IAtmCookie& cookie ) const {
    return atm_string::getString( cookie, atm_string::id_push_title );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServicePush::getPrompt( const IAtmCookie& cookie ) const {
    return atm_string::getString( cookie, atm_string::id_push_prompt );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
int AtmServicePush::getOptionsCount( const IAtmCookie& cookie ) const {
    return 0;
}
 
 
////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServicePush::getOption( const IAtmCookie& cookie,
                                       int index ) const {
   return NULL;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const int AtmServicePush::getSelectedOption( const IAtmCookie& cookie ) const {
    return ( ( getCash( cookie ) < getAmount( cookie ) ) ? 1 : 0 );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const int AtmServicePush::getCash( const IAtmCookie& cookie ) const {
    int result = 0;
    int idx = 0;
    std::string banknote;
    while ( cookie.getProperty( IAtmCookie::eCashIn, idx++, banknote ) ) {
        result += atoi( banknote.c_str() );
    }
    return result;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const int AtmServicePush::getAmount( const IAtmCookie& cookie ) const {
    const char* amount = cookie.getProperty( IAtmCookie::eAmountVal );
    int result = -1;
    if ( (NULL != amount ) && ( atoi( amount ) > 0 ) ) {
        result = atoi( amount );
    }    
    return result;
}

}// namespace atm