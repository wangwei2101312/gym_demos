////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __ATMBANK_H__
#define __ATMBANK_H__

#include "atmbankaccount.h"
#include <list>

namespace atm {

/// Forward declarations
class IAtmStrategy;

/** \brief ATM bank class, stores accounts and card data
  */ 
class AtmBank
{
public:
    /** \brief Card structure, paired with operating strategy
      */ 
    struct Card {
        /** \brief Constructor
          */ 
        Card( const char* id,
              const char* p,
              int mIn,
              int mOut )
        : cardid( id )
        , pin( p )
        , maxIn( mIn )
        , maxOut( mOut ) {
        }

        ~Card() {}

        std::string     cardid;
        std::string     pin;  
        int             maxIn;
        int             maxOut;
    };

    /** \brief Account structure, paired with cards
      */ 
    struct Account {
        Account( const AtmBankAccount& acc ) 
        : account( acc ) {
        }

        ~Account() {}

        AtmBankAccount      account;
        std::list< Card >   cards;
    };

protected:
    /** \brief Constructor
      */ 
    AtmBank();

    /** \brief Destructor
      */ 
     ~AtmBank();

public:
    /** \brief Returns bank instance
      * \return pointer to a valid bank instance 
      */ 
    static AtmBank* getInstance();

public:
    /** \brief Checks whether card ID is valid
      * \param[in] cardId - card code
      * \return true when succeed. otherwise false
      */ 
    bool checkCardId( const char* cardid ) const;

    /** \brief Checks whether card ID and PIN are valid
      * \param[in] cardId - card code
      * \param[in] pin - pin code
      * \return true when succeed. otherwise false
      */ 
    bool checkPin( const char* cardid,
                   const char* pin ) const;

    /** \brief Generates a move on the account. 
      *        Negative value means income, positive value means payment
      * \param[in] cardId - card code
      * \param[in] pin - pin code
      * \param[in] value - amount to be processed
      * \param[in] simulate - when true, the move is simulated only, otherwise executed
      * \return true when succeed. otherwise false
      */ 
    bool extract( const char* cardid, 
                  const char* pin,
                  int value,
                  bool simulate );

    /** \brief Provides account information
      * \param[in] cardId - card code
      * \param[in] pin - pin code
      * \param[in] type - information type: 
      *                     0  = balance only
      *                     1  = balance and credit
      *                     2  = balance and extras of last 10 transactions
      * \param[in] info - information 
      * \return true when succeed. otherwise false
      */ 
    bool showInfo( const char* cardid, 
                   const char* pin,
                   char type,
                   std::vector< int >& info );

protected:
    /** \brief Retrieves account by Card Id
      * \param[in] cardId - card code
      * \param[in] pin - pin code
      * \param[out] account - account access
      * \param[out] maxIn - income limits per transaction
      * \param[out] maxOut - payment limits per transaction
      * \return true when succeed. otherwise false
      */ 
    bool findAccount( const char* cardid,
                      const char*& pin,
                      AtmBankAccount*& account, 
                      const int*& maxIn,
                      const int*& maxOut ) const;

public:
    /** \brief Dumps bank current status
      * \param[in] listener - listener
      */ 
    void dump( IAtmDumpListener& listener ) const;

protected:
    /// Accounts of bank
    mutable std::list< Account >  m_accounts;

private:
    // Disabled
    AtmBank( AtmBank& );
    AtmBank& operator = ( const AtmBank& );

};

}// namespace atm

#endif// __ATMBANK_H__
