////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atmservice_pin.h"
#include "iatmcookie.h"
#include "atmstrings.h"
#include <stdlib.h>
#include <string>
#include <string.h>

namespace atm {

namespace {
    // Pin service options count
    const int   s_option_count  = 0;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServicePin::AtmServicePin( DispatchBank bank )
: m_bank( bank ) {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServicePin::~AtmServicePin() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmServicePin::resetOptions( IAtmCookie& cookie,
                                  IAtmListener& listener ) {
    cookie.setProperty( IAtmCookie::ePin, NULL );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServicePin::execute( IAtmCookie& cookie,
                             IAtmListener& listener,
                             const char* input,
                             const char*& result ) {
    // Verify if pin code 
    bool success = false;
    result = atm_string::getString( cookie, atm_string::id_pin_invalid );
    if ( ( NULL != input ) && ( strlen(input) == 4 ) ) {
        if ( atoi( input ) > 0 ) {
            // Ask bank to verify the pin code
            if ( ( *m_bank )( cookie.getProperty( IAtmCookie::eCardId ), input ) ) {
                // Add pin to cookie
                cookie.setProperty( IAtmCookie::ePin, input );
                result = atm_string::getString( cookie, atm_string::id_pin_valid );
                success = true;
            }
            else {
                result = atm_string::getString( cookie, atm_string::id_pin_rejected );
            }
        }
    }
    return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServicePin::getName( const IAtmCookie& cookie ) const {
    return atm_string::getString( cookie, atm_string::id_pin_title );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServicePin::getPrompt( const IAtmCookie& cookie ) const {
    return atm_string::getString( cookie, atm_string::id_pin_prompt );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
int AtmServicePin::getOptionsCount( const IAtmCookie& cookie ) const {
    return s_option_count;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServicePin::getOption( const IAtmCookie& cookie,
                                      int index ) const {
    return NULL;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const int AtmServicePin::getSelectedOption( const IAtmCookie& cookie ) const {
    // ATM service pin has no selected options
    return -1;
}


}// namespace atm