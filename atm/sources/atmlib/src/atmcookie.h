////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef  __ATMCOOKIE_H__
#define  __ATMCOOKIE_H__

#include "iatmcookie.h"
#include <string>
#include <map>

namespace atm {

/// Forward declarations
class IAtmStrategy;


/** \brief ATM state machine interface class
  */ 
class AtmCookie : protected IAtmCookie
{
    friend class AtmFactory;
protected:
    /** \brief Constructor
      * \param[in] atmStore - ATM store
      * \param[in] minAmount - minimum amount to operate
      * \param[in] maxAmount - maximum amount to operate
      * \param[in] strategy - strategy to operate ATM store
      */ 
    AtmCookie( std::map<int, int>& atmStore,
               int& minAmount, 
               int& maxAmount,
               IAtmStrategy* strategy );

    /** \brief Virtual destructor
      */ 
    virtual ~AtmCookie();

protected:
    /** \brief Set property for cookie
      * \param[in] id - property identifier (see EProperty)
      * \param[in] val - property value
      */ 
    virtual void setProperty( EProperty id, 
                              const char* val );

    /** \brief Appends to cookie property
      * \param[in] id - property identifier (see EProperty)
      * \param[in] val - property value
      */ 
    virtual void addProperty( EProperty id, 
                              const char* val );

    /** \brief Get property from cookie
      * \param[in] id - property identifier (see EProperty)
      * \returns property value
      */ 
    virtual const char* getProperty( EProperty id ) const;

    /** \brief Get a property from cookie property array
      * \param[in] id - property identifier (see EProperty)
      * \param[in] index - index of property array
      * \param[out] val - string where to return
      * \returns true, when succeed, otherwise false
      */ 
    virtual bool getProperty( EProperty id, 
                              int index,
                              std::string& val ) const;

    /** \brief Get ATM operating strategy
      * \returns strategy interface
      */ 
    virtual IAtmStrategy* getStrategy() const;

public:
    /** \brief Provides access to ATM store
      * \param[in]  cookie - cookie instance
      * \param[out] atmStore - ATM store
      * \param[out] minAmount - minimum amount to operate
      * \param[out] maxAmount - maximum amount to operate
      */ 
    static void getStore( AtmCookie& cookie,
                          std::map<int, int>*& atmStore,
                          int& minAmount, 
                          int& maxAmount );

protected:
    /// Cookie properties
    std::string         m_prop[ eLast ];

private:
    // Disabled
    AtmCookie( AtmCookie& );
    AtmCookie& operator = ( const AtmCookie& );

private:
    std::map<int, int>&     m_store;
    int&                    m_maxIn;         
    int&                    m_maxOut;         
    IAtmStrategy*           m_strategy;
};

}// namespace atm

#endif// __ATMCOOKIE_H__
