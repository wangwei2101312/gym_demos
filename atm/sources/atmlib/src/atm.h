////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __ATM_H__
#define __ATM_H__

#include <iatm.h>
#include <map>

namespace atm {

/// Forward declarations 
class IAtmService;    
class IAtmCookie;
class IAtmListener;
class IAtmDumpListener;
    
/** \brief ATM class
  *  implements an ATM interface
  */ 
class Atm : public IAtm {

    /** \brief Access for ctor/dtor is preserved for AtmFactory
      */ 
    friend class AtmFactory;
public:
    typedef std::map<int, int > TCacheStore;

protected:
    /** \brief Constructor
      * \param[in] listener - ATM listener object
      * \param[in] strategy - ATM store operating strategy
      */ 
    Atm( IAtmListener& listener,
         IAtm::EStrategy strategy );
    
    /** \brief Virtual destructor
      */ 
    virtual ~Atm();
	
public:
    /** \brief Initialize the ATM
      */ 
    virtual void init();
    
    /** \brief Go back to the previous stage
      */ 
    virtual void back();
        
    /** \brief Go to the next stage
      * \param[in] input - user input
      * \param[out] result - ATM answer
      */ 
    virtual void next( const char* input,
                       const char*& result ); 

    /** \brief Exit, with canceling any pending operations
      */ 
    virtual void exit();
    
public:
    /** \brief Checks if ATM is in active mode
      * \returns true, when ATM session is valid, otherwise false
      */ 
    virtual bool isActive() const;

    /** \brief Checks if ATM has a valid previous state
      * \returns true, when ATM previous state is valid, otherwise false
      */ 
    virtual bool hasBack() const;

    /** \brief ATM's current state name
      * \returns the name of current state
      */ 
    virtual const char* getStateName() const;    
    
    /** \brief ATM's current state's prompt
      * \returns the prompt of the current state
      */ 
    virtual const char* getStatePrompt() const;    

    /** \brief ATM's current state options count
      * \returns the prompt for the current state
      */ 
    virtual int getStateOptionsCount() const;    
    
    /** \brief ATM's current state option by index
      * \param[in] index - option index
      * \returns option text
      */ 
    virtual const char* getStateOption( int index ) const;    

public:
    /** \brief Dumps ATM current status
      * \param[in] listener - listener
      */ 
    void dump( IAtmDumpListener& listener ) const;

protected:
    // Cache store
    TCacheStore         m_cacheStore;
    // ATM state handler service
    IAtmService*        m_service;
    // Cookie for storing data
    IAtmCookie*         m_cookie;
    // Event listener object
    IAtmListener&       m_listener;
    // Max outgoing amount per transaction
    int                 m_maxIn;
    // Max incoming amount per transaction
    int                 m_maxOut;
    // Max incoming amount per transaction
    EStrategy           m_strategy;

private:
    // Disable auto-generated copy constructor and assignment operator
    Atm( const Atm& );
    Atm& operator =( const Atm& );
  
};

}// namespace atm

#endif// ATM_H
