////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atmservice_card.h"
#include "iatmcookie.h"
#include <iatmlistener.h>
#include "atmstrings.h"
#include <stdlib.h>
#include <string>
#include <string.h>

namespace atm {

namespace {
    // Card service options count
    const int   s_option_count = 0;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServiceCard::AtmServiceCard( DispatchBank bank )
: m_bank( bank ) {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmServiceCard::~AtmServiceCard() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmServiceCard::resetOptions( IAtmCookie& cookie,
                                   IAtmListener& listener ) {
    if ( NULL != cookie.getProperty( IAtmCookie::eCardId ) ) {
        // Execute action assigned to property
        std::string action( atm_string::getString( cookie, atm_string::id_eject_card ) );
        action += cookie.getProperty( IAtmCookie::eCardId );
        listener.prompt( atm_string::getString( cookie, atm_string::id_remove_card ), action.c_str() );
        // Reset property
        cookie.setProperty( IAtmCookie::eCardId, NULL );
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmServiceCard::execute( IAtmCookie& cookie,
                              IAtmListener& listener,
                              const char* input,
                              const char*& result ) {
    bool success = false;
    result = atm_string::getString( cookie, atm_string::id_card_invalid );
    if ( ( NULL != input ) && ( strlen( input ) == 10 ) ) {
        // Ask bank to verify the card's validity
        cookie.setProperty( IAtmCookie::eCardId, input );
        if ( ( *m_bank )( input ) ) {
            result = atm_string::getString( cookie, atm_string::id_card_loaded );
            success = true;
        }
        else {
            result = atm_string::getString( cookie, atm_string::id_card_rejected );
        }
    }
    return success;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServiceCard::getName( const IAtmCookie& cookie ) const {
    return atm_string::getString( cookie, atm_string::id_card_title );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServiceCard::getPrompt( const IAtmCookie& cookie ) const {
    return atm_string::getString( cookie, atm_string::id_card_prompt );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
int AtmServiceCard::getOptionsCount( const IAtmCookie& cookie ) const {
    return s_option_count;
}
 
 
////////////////////////////////////////////////////////////////////////////////////////////////////
const char* AtmServiceCard::getOption( const IAtmCookie& cookie,
                                       int index ) const {
    return NULL;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const int AtmServiceCard::getSelectedOption( const IAtmCookie& cookie ) const {
    // ATM service card has no selected options
    return -1;
}

}// namespace atm