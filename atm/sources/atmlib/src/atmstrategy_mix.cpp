////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atmstrategy_mix.h"

namespace atm {

    
////////////////////////////////////////////////////////////////////////////////////////////////////
AtmStrategyMixed::AtmStrategyMixed() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmStrategyMixed::~AtmStrategyMixed() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
int AtmStrategyMixed::extract( std::map<int, int >& store,
                               int val ) {
    // Extract a bank-note 
    std::map<int, int >::reverse_iterator it;
    std::map<int, int >::reverse_iterator candidate;
    bool hasCandidate = false;
    for ( it = store.rbegin(); it != store.rend(); ++it ) {
        if ( ( it->first <= val )  && ( it->second > 0 ) ) {
            if ( hasCandidate ) {
                if ( ( candidate->first * candidate->second ) >
                     ( it->first * it->second ) ) {
                        // Previous candidate looks better, in order to keep ATM balanced
                        continue;
                }
            }
            candidate = it;
            hasCandidate = true;
        }
    }
    // Check if valid
    if ( hasCandidate ) {
        candidate->second--;
        return candidate->first;
    }
    return 0;
}

}// namespace atm
