////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atm.h"
#include <iatmlistener.h>
#include <iatmdumplistener.h>
#include "iatmcookie.h"
#include "iatmservice.h"
#include "atmfactory.h"
#include "atmserviceprovider.h"
#include <stdlib.h>
#include <stdio.h>

namespace atm {

    
namespace {
    // Fatal error for ATM display
    const char* s_fatal_error   = "#ERROR"; 
    // Local messages
    const char* s_remove_cache  = "Please remove your money";
    const char* s_remove_card   = "Please remove your card";
    const char* s_remove_ticket = "Please remove your ticket";
    const char* s_eject_card    = ">> EJECTED: CARD = ";
    const char* s_eject_cash    = ">> EJECTED: CASH = ";
    const char* s_eject_ticket  = ">> EJECTED: TICKET = ";
}


////////////////////////////////////////////////////////////////////////////////////////////////////
Atm::Atm( IAtmListener& listener,
          IAtm::EStrategy strategy )
: m_cacheStore()
, m_service( NULL )
, m_cookie( NULL )
, m_listener( listener )
, m_maxIn( 100 )
, m_maxOut( 1500 )
, m_strategy( strategy ){
    /// Initialize cache store
    m_cacheStore[1] =  0;
    m_cacheStore[5] =  200;
    m_cacheStore[10] = 10000;
    m_cacheStore[50] = 1000;
    m_cacheStore[100] = 2000;
    m_cacheStore[200] = 0;
    m_cacheStore[500] = 100;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
Atm::~Atm() {
    exit();
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void Atm::init() {
    // Initialize ATM
    if ( NULL != m_cookie ) {
        exit();
    }
    m_cookie = AtmFactory::createCookie( m_cacheStore, m_maxIn, m_maxOut, m_strategy );
    m_service = AtmServiceProvider::getNextService( NULL, *m_cookie, m_listener, true );
}
 
 
////////////////////////////////////////////////////////////////////////////////////////////////////
void Atm::back() {
    m_service = AtmServiceProvider::getPrevService( m_service, *m_cookie, m_listener );
}

        
////////////////////////////////////////////////////////////////////////////////////////////////////
void Atm::next( const char* input, 
                const char*& result ) {
    if ( NULL != m_service ) {
        // Launch the current service, than switch to next state handler (when execution succeed)
        do {
            bool success = m_service->execute( *m_cookie, m_listener, input, result );
            m_service = AtmServiceProvider::getNextService( m_service, *m_cookie, m_listener, success );
        } while ( ( NULL != m_service ) && ( NULL == m_service->getPrompt( *m_cookie ) ) ); 
    }
    else {
        exit();
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void Atm::exit() {
    if ( NULL != m_cookie ) {
        AtmServiceProvider::exitServices( *m_cookie, m_listener );
        AtmFactory::destroyCookie( m_cookie );
    }
    m_service = NULL;
}
    

////////////////////////////////////////////////////////////////////////////////////////////////////
bool Atm::isActive() const {
    return ( NULL != m_service ); 
}
    

////////////////////////////////////////////////////////////////////////////////////////////////////
bool Atm::hasBack() const {
    IAtmService *prev = AtmServiceProvider::getPrevService( m_service, *m_cookie, m_listener );
    return ( ( NULL != prev ) && ( prev != m_service ) );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* Atm::getStateName() const {
    return ( ( NULL != m_service ) ? m_service->getName( *m_cookie ) : s_fatal_error );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* Atm::getStatePrompt() const {
    return ( ( NULL != m_service ) ? m_service->getPrompt( *m_cookie ) : s_fatal_error );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
int Atm::getStateOptionsCount() const {
    return ( ( NULL != m_service ) ? m_service->getOptionsCount( *m_cookie ) : -1 );
}
    

////////////////////////////////////////////////////////////////////////////////////////////////////
const char* Atm::getStateOption(int index) const {
    return ( ( NULL != m_service ) ? m_service->getOption(  *m_cookie, index ) : s_fatal_error );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void Atm::dump( IAtmDumpListener& listener ) const {
    int pos = 0;
    char key[64];
    char val[64];
    listener.dump( "ATM status", NULL, 0 );
    for ( TCacheStore::const_iterator it = m_cacheStore.begin(); it != m_cacheStore.end(); ++it ) {
        sprintf( key, "%d. Banknotes <%u>", ++pos, it->first );
        sprintf( val, "%u", it->second );
        listener.dump( key, val, 1 );
    }
}
    
}// namespace atm
