////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atmstrings.h"
#include "iatmcookie.h"
#include <stdlib.h>

namespace atm {

namespace atm_string{

////////////////////////////////////////////////////////////////////////////////////////////////////
// English strings
const char* atm_strings_en[] = {
/*  0*/ "",
/*  1*/ "VERIFYING CARD",
/*  2*/ "Please insert your card (a 10 character length card id)",
/*  3*/ "No card inserted. Please retry",
/*  4*/ "Card is not valid, or not supported by our bank",
/*  5*/ "Card loaded ... ",
/*  6*/ "Please remove your money",
/*  7*/ "Please remove your card",
/*  8*/ "Please remove your ticket",
/*  9*/ "[ PHYSICAL EVENT] >> EJECTED: CARD = ",
/* 10*/ "[ PHYSICAL EVENT] >> EJECTED: CASH = ",
/* 11*/ "[ PHYSICAL EVENT] >> EJECTED: TICKET = ",
/* 12*/ "Card information", 
/* 13*/ "PIN CODE VERIFICATION",
/* 14*/ "Please insert your PIN (4 digits)",
/* 15*/ "Invalid PIN code. Please specify a 4 digit number",
/* 16*/ "PIN accepted ... ",
/* 17*/ "PIN code rejected. Please try again",
/* 18*/ "CARD FILL UP",
/* 19*/ "Please select the desired amount",
/* 20*/ "Please type-in the desired amount",
/* 21*/ " <1>  50\0 <2>  100\0 <3>  200\0 <4>  500\0 <5> 1000\0 <6> Another\0",
/* 22*/ "Invalid amount. Please retry",
/* 23*/ "Amount selected ... ",
/* 24*/ "Manual input selected ... ",
/* 25*/ "ATM TRANSACTION TYPES",
/* 26*/ "Please select the desired transaction",
/* 27*/ "Invalid option. Please retry",
/* 28*/ "Transaction selected ... ",
/* 29*/ " <1>  Cash retrieval\0 <2>  Card fill-up\0 <3>  Account info\0",
/* 30*/ "CASH RETRIEVAL",
/* 31*/ "Please select the desired amount",
/* 32*/ "Please type-in the desired amount",
/* 33*/ " <1>  100\0 <2>  200\0 <3>  300\0 <4>  500\0 <5> 1000\0 <6> Another\0",
/* 34*/ "Invalid amount. Please retry",
/* 35*/ "Amount selected ... ",
/* 36*/ "Manual input selected ... ",
/* 37*/ "Card fill-up not confirmed by service provider",
/* 38*/ "Cash retrieval not confirmed by service provider",
/* 39*/ "Invalid option. Please retry",
/* 40*/ "Invalid option. Please retry",
/* 41*/ "PROCESSING CASH INSERTION",
/* 42*/ "PROCESSING CASH EXTRACTION",
/* 43*/ "Please insert cash: ",
/* 44*/ "Remained: ",
/* 45*/ "Banknote not accepted: ",
/* 46*/ "Could not execute the requested transaction",
/* 47*/ "Could not execute the requested transaction",
/* 48*/ "Transaction succeed",
/* 49*/ "Transaction succeed",
/* 50*/ "Please specify whether you want a receipt for: ",
/* 51*/ " <1>  Screen\0 <2>  Printer\0 <3>  No\0",
/* 52*/ "[ PHYSICAL EVENT] << UNLOCKED CASH = ",
/* 53*/ "Transaction canceled",
/* 54*/ "Banknote accepted ... ",
/* 55*/ "Account information",
/* 56*/ "Please specify whether you want : ",
/* 57*/ " <1>  Balance\0 <2>  Credit\0 <3>  Movements\0",
};


////////////////////////////////////////////////////////////////////////////////////////////////////
// Romanian strings
const char* atm_strings_ro[] = {
    /*  0*/ "",
    /*  1*/ "VERIFICARE CARD",
    /*  2*/ "Va rog sa introduceti cardul (un cod de 10 caractere)",
    /*  3*/ "Nu s-a introdus bine cardul. Incercati din nou va rog",
    /*  4*/ "Cardul nu este valid, sau nu este suportat de banca",
    /*  5*/ "Card citit ... ",
    /*  6*/ "Va rog sa retrageti numararul",
    /*  7*/ "Va rog sa retrageti cardul",
    /*  8*/ "Va rog sa retrageti bonul",
    /*  9*/ "[ EVENIMENT FIZIC] >> EJECTAT: CARD = ",
    /* 10*/ "[ EVENIMENT FIZIC] >> EJECTAT: BANCNOTE = ",
    /* 11*/ "[ EVENIMENT FIZIC] >> EJECTAT: BON = ",
    /* 12*/ "Informatie de card", 
    /* 13*/ "VERIFICARE PIN",
    /* 14*/ "Introduceti codul pin (4 cifre)",
    /* 15*/ "Codul pin este invalid. Introduceti un cod din 4 cifre",
    /* 16*/ "Codul pin acceptat ... ",
    /* 17*/ "Codul pin neacceptat. Incercati din nou va rog",
    /* 18*/ "INCARCARE CARD CU BANI",
    /* 19*/ "Please select the desired amount",
    /* 20*/ "Please type-in the desired amount",
    /* 21*/ " <1>  50\0 <2>  100\0 <3>  200\0 <4>  500\0 <5> 1000\0 <6> Another\0",
    /* 22*/ "Invalid amount. Please retry",
    /* 23*/ "Amount selected ... ",
    /* 24*/ "Manual input selected ... ",
    /* 25*/ "ATM TUPURI DE TRANZACTIE",
    /* 26*/ "Please select the desired transaction",
    /* 27*/ "Invalid option. Please retry",
    /* 28*/ "Transaction selected ... ",
    /* 29*/ " <1>  Cash retrieval\0 <2>  Card fill-up\0 <3>  Account info\0",
    /* 30*/ "ELIBERARE BANI",
    /* 31*/ "Please select the desired amount",
    /* 32*/ "Please type-in the desired amount",
    /* 33*/ " <1>  100\0 <2>  200\0 <3>  300\0 <4>  500\0 <5> 1000\0 <6> Another\0",
    /* 34*/ "Invalid amount. Please retry",
    /* 35*/ "Amount selected ... ",
    /* 36*/ "Manual input selected ... ",
    /* 37*/ "Card fill-up not confirmed by service provider",
    /* 38*/ "Cash retrieval not confirmed by service provider",
    /* 39*/ "Invalid option. Please retry",
    /* 40*/ "Invalid option. Please retry",
    /* 41*/ "PROCESSING CASH INSERTION",
    /* 42*/ "PROCESSING CASH EXTRACTION",
    /* 43*/ "Please insert cash: ",
    /* 44*/ "Remained: ",
    /* 45*/ "Banknote not accepted: ",
    /* 46*/ "Could not execute the requested transaction",
    /* 47*/ "Could not execute the requested transaction",
    /* 48*/ "Transaction succeed",
    /* 49*/ "Transaction succeed",
    /* 50*/ "Please specify whether you want a receipt for: ",
    /* 51*/ " <1>  Screen\0 <2>  Printer\0 <3>  No\0",
    /* 52*/ "[ PHYSICAL EVENT] << UNLOCKED CASH = ",
    /* 53*/ "Transaction canceled",
    /* 54*/ "Banknote accepted ... ",
    /* 55*/ "Account information",
    /* 56*/ "Please specify whether you want : ",
    /* 57*/ " <1>  Balance\0 <2>  Credit\0 <3>  Movements\0",
};


////////////////////////////////////////////////////////////////////////////////////////////////////
const char* getString( const IAtmCookie& cookie, 
                       EStringId stringId ) {
    // The default language
    if ( NULL != cookie.getProperty( IAtmCookie::eLanguage ) ) {
        switch( atoi( cookie.getProperty( IAtmCookie::eLanguage ) ) ) {
        case 2:
            // Romanian language
            return atm_strings_ro[ stringId ];
            break;
        case 3:
            // Hungarian language
            return atm_strings_en[ stringId ];
            break;
        }
    }
    // Default (English) language
    return atm_strings_en[ stringId ];
}


}// namespace atm_string

}// namespace atm