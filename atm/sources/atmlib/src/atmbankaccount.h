////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __ATMBANKACCOUNT_H__
#define __ATMBANKACCOUNT_H__

#include <string>
#include <list>
#include <vector>

namespace atm {

/// Forward declarations
class IAtmDumpListener;

/** \brief ATM state machine interface class
  */ 
class AtmBankAccount
{
    friend class AtmBank;
protected:
    /** \brief Constructor
      * \param[in] owner - the owner of account
      * \param[in] balance - initial balance
      * \param[in] debit - maximum of debit
      * \param[in] credit - maximum of credit
      */ 
    AtmBankAccount( const char* owner, 
                    int balance,
                    int debit,
                    int credit);

    /** \brief Copy constructor
      * \param[in] other - where from to copy
      */ 
    AtmBankAccount( const AtmBankAccount& other );

    /** \brief Destructor
      */ 
    ~AtmBankAccount();

public:
    /** \brief Extract a value
      * \param[in] value - value to extract, or add (negative value means an add, positive value means an extract)
      * \param[in] simulate - operation is simulated only, to verify the validity
      * \return true, when  succeed, otherwise false
      */ 
    bool extract( int value,
                  bool simulate );

    /** \brief Show account info
      * \param[in] info - array for movements
      * \param[in] type - information type: 
      *                     0  = balance only
      *                     1  = balance and credit
      *                     2  = balance and extras of last 10 transactions
      * \return true, when  succeed, otherwise false
      */ 
    bool showInfo( std::vector<int >& info,
                   char type );

    /** \brief Get the balance of account
      * \return balance of account
      */ 
    int getBalance() const;

    /** \brief Get the maximum credit
      * \return credit value
      */ 
     int getCredit() const;

    /** \brief Get the maximum debit
      * \return credit value
      */ 
     int getDebit() const;

    /** \brief Get the owner of account
      * \return name of owner
      */ 
    const char* getOwner() const;

public:
    /** \brief Dumps bank current status
      * \param[in] listener - listener
      * \param[in] level - indention level
      */ 
    void dump( IAtmDumpListener& listener,
               int level ) const;

private:
    // Disabled
    AtmBankAccount& operator = ( const AtmBankAccount& );

protected:
    /// Owner of account
   std::string      m_owner;
   /// Balance of account
   std::list<int>   m_ins;
   std::list<int>   m_outs;
   /// Maximum debit to apply
   int              m_maxDebit;
   /// Maximum credit to apply
   int              m_maxCredit;
};

}// namespace atm

#endif// __ATMBANKACCOUNT_H__
