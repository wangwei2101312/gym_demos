////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "atm_terminal.h"



/** \brief Entry point in ATM terminal
  *  Accepts command line arguments:
  *         '-a' = ascendant ATM strategy, 
  *         '-d' = descendant ATM strategy, 
  *         '-b' = balanced ATM strategy
  * \param[in] argc - command lines parameters count
  * \param[in] argv - command lines parameters array
  * \return 0 when succeed, otherwise false)
  */ 
int main( int argc, 
          char *argv[])
{
    atm::IAtm::EStrategy option = atm::IAtm::eS_Balanced;
    if ( argc == 2 ) {
        if ( 0 == strcmp( argv[1], "-a" ) ) {
            std::cout << "Ascendant ATM strategy selected" << std::endl << std::endl;
            option = atm::IAtm::eS_Ascendant;
        }
        else if ( 0 == strcmp( argv[1], "-d" ) ) {
            std::cout << "Descendant ATM strategy selected" << std::endl << std::endl;
            option = atm::IAtm::eS_Descendant;
        }
        else if ( 0 == strcmp( argv[1], "-b" ) ) {
            std::cout << "Balanced ATM strategy selected" << std::endl << std::endl;
            option = atm::IAtm::eS_Balanced;
        }
        else if ( 0 == strcmp( argv[1], "-v" ) ) {
            std::cout << "ATM Demo application, version 1.01" << std::endl;
            std::cout << "3-clause BSD License" << std::endl;
            std::cout << "Copyright (C) Richter Zoltan, for educational purpose" << std::endl;
            return 0;
        }
        else if ( 0 == strcmp( argv[1], "-h" ) ) {
            std::cout << "Usage" << std::endl;
            std::cout << "  atm-terminal [OPTIONS] " << std::endl << std::endl;
            std::cout << "ATM Demo application, please see the API documentation for developers " << std::endl;
            std::cout << "For users: use the secret and functional keys displayed on each working screen" << std::endl << std::endl;
            std::cout << "OPTIONS: " << std::endl;
            std::cout << "  -h    display help for usage " << std::endl;
            std::cout << "  -a    ascendant ATM storage strategy, tries to extract higher banknotes first " << std::endl;
            std::cout << "  -d    descendant ATM storage strategy, tries to extract lower banknotes first " << std::endl;
            std::cout << "  -b    balanced ATM storage strategy, tries to keep ATM storage in balance " << std::endl;
            std::cout << "  -v    version " << std::endl;
            return 0;
        }
        else {
            std::cout << "Invalid command line options, Please use '-h' for help";
            return -1;
        }
    }

    // Invoke atm terminal
    AtmTerminal atm;
    atm.start( option );

    return 0;
}
