////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "atm_terminal.h"
#include <iatmfactory.h>
#include <iatmdumper.h>
#include <iostream>
#include <stdlib.h>
#include <string>


////////////////////////////////////////////////////////////////////////////////////////////////////
AtmTerminal::AtmTerminal() {
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmTerminal::start( atm::IAtm::EStrategy strategy ) {
    atm::IAtm* atm = atm::IAtmFactory::getInstance()->getAtm( *this, 0, strategy );
    while ( runSession( atm ) );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmTerminal::prompt( const char* msg, 
                          const char* action ) {
    if ( NULL != msg ) {
        std::cout << msg << std::endl;
    }
    if ( NULL != action ) {
        std::cout << action << std::endl;
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmTerminal::dump( const char* key, 
                       const char* val,
                       int level ) {
    while ( level-- ) {
        std::cout << "  ";
    }
    if ( NULL != key ) {
        std::cout << key << ": " ;
    }
    if ( NULL != val ) {
        std::cout << val ;
    }
    std::cout << std::endl;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmTerminal::dumpBank() {
    char c;
    do {
        std::cout << "==========================================================================="  << std::endl;
        std::cout << "  Dump bank content " << std::endl;
        std::cout << "---------------------------------------------------------------------------"  << std::endl;
        atm::IAtmFactory::getInstance()->getAtmDumper()->dumpBank( *this );
        std::cout << "==========================================================================="  << std::endl;
        std::cout << "Repeat?(y/n) "  << std::endl;
        std::cin >> c;
    } while( c == 'y' );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
void AtmTerminal::dumpAtm( atm::IAtm* atm ) {
    char c;
    do {
        std::cout << "==========================================================================="  << std::endl;
        std::cout << "  Dump ATM content " << std::endl;
        std::cout << "---------------------------------------------------------------------------"  << std::endl;
        atm::IAtmFactory::getInstance()->getAtmDumper()->dumpAtm( *this, atm );
        std::cout << "==========================================================================="  << std::endl;
        std::cout << "Repeat?(y/n) "  << std::endl;
        std::cin >> c;
    } while( c == 'y' );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool AtmTerminal::runSession( atm::IAtm* atm ) {
    bool isWorking = true;
    std::string command;
    const char* atm_answer = NULL;
        
    // Operate Atm
    atm->init();
    // Cycle over
    while( atm->isActive() ) {
        std::cout << "==========================================================================="  << std::endl;
        std::cout << "  ATM Demo application " << std::endl;
        std::cout << "---------------------------------------------------------------------------"  << std::endl;
        std::cout << std::endl;
        // Ask for continue
        std::cout << atm->getStateName() << std::endl;
        std::cout << "----------------------------------------------------------------------"  << std::endl;
        std::cout << "Secret keys: | Power off<Ctrl+X> | Bank-log<Ctrl+D> | ATM-log<Ctrl+A> |" << std::endl;
        std::cout << "-----------------------------------------------------------------------"  << std::endl;
        std::cout << "Public keys: | Exit<Ctrl+E>  | Next<ENTER>      |";
        if ( atm->hasBack() ) {
            std::cout << " Back<Ctrl+B>    |";
        }
        std::cout << std::endl;
        // Display options
        for (int i = 0; i < atm->getStateOptionsCount(); i++ ) {
            std::cout << (i + 1) << ". " << atm->getStateOption( i ) << std::endl;
        }
        std::cout << std::endl;
        std::cout << atm->getStatePrompt() << ": ";
        std::cin >> command;
        // Check last character
        if ( 4 == *command.rbegin() ) {
            // Bank key found
            dumpBank();
            continue;
        }
        else if ( 1 == *command.rbegin() ) {
            // ATM key found
            dumpAtm( atm );
            continue;
        }
        else if ( 24 == *command.rbegin() ) {
            // Power off key found
            isWorking = false;
            break;
        }            
        else if ( 5 == *command.rbegin() ) {
            // Exit key found
            break;
        }            
        else if ( 2 == *command.rbegin() ) {
            // Back key found
            atm->back();
            continue;
        }
        atm->next( command.c_str(), atm_answer );
        if ( NULL != atm_answer ) {
            std::cout << atm_answer << std::endl;
        }
        else {
            std::cout << std::endl;
        }
        std::cout << std::endl;
        std::cout << std::endl;
    }
    // End session
    atm->exit();
    std::cout << std::endl;
    if (isWorking ) {
        std::cout << "************* GOOD BYE! ****************" << std::endl;
    }
    else {
        std::cout << "**********OUT OF SERVICE!**************" << std::endl;
    }
    std::cout << std::endl;
    return isWorking;
}
