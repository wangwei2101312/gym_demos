////////////////////////////////////////////////////////////////////////////////////////////////////
// ATM Demo license
// 
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install, copy or use the software.
//  
//  License Agreement
//  For ATM demo 
//  (3-clause BSD License)
//
//  Copyright (c) 2017 
//  Richter Zoltan
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the <organization> nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __ATM_TERMINAL_H__
#define __ATM_TERMINAL_H__

#include <iatm.h>
#include <iatmlistener.h>
#include <iatmdumplistener.h>


/** \brief ATM terminal class, client for ATM library
  */ 
class AtmTerminal : public atm::IAtmListener,
                    public atm::IAtmDumpListener {
public:
    /** \brief Constructor
      */ 
    AtmTerminal();

public:
    /** \brief Start the ATM
      * \param[in] strategy - terminal strategy option 
      *                       accepts values (0 = BALANCED(default). 1 = ASC, 2 DESC)
      */ 
    void start( atm::IAtm::EStrategy strategy );

protected:
    /** \brief Prompt message from IAtmListener
      * \param[in] msg - message to display
      * \param[in] action - physical action executed by the ATM
      */ 
    virtual void prompt( const char* msg, 
                         const char* action );

    /** \brief Dump message from IAtmDumpListener
      * \param[in] key - key of dumped message
      * \param[in] value - value of dumped message
      * \param[in] level - indention level of dumped message
      */ 
    virtual void dump( const char* key, 
                       const char* val,
                       int level );

protected:
    /** \brief Dump actual content of bank, results are coming via IAtmDumpListener
      */ 
    void dumpBank();

    /** \brief Dump actual content of ATM, results are coming via IAtmDumpListener
      * \param[in] atm - ATM instance to get dumped
      */ 
    void dumpAtm( atm::IAtm* atm );

    /** \brief Runs an ATM session
      * \param[in] atm - ATM instance
      */ 
    bool runSession( atm::IAtm* atm );

};

#endif //__ATM_TERMINAL_H__
 